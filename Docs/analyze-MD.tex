% Manual for the ANALIZE-MD program
%
% To generate the printed version:
%
% latex analize-MD
% latex analize-MD
% [ dvips analize-MD ] 
%
%   Pablo Ordejon    Feb. 2008   
%


\documentstyle[11pt]{article}

\tolerance 10000
\textheight 22cm
\textwidth 16cm
\oddsidemargin 1mm
\topmargin -15mm

\baselineskip=14pt
\parskip 5pt
\parindent 1em

\begin{document}

% TITLE PAGE --------------------------------------------------------------

\begin{titlepage}

\begin{center}

\centerline{ }

\vspace{1cm}

{\huge {\sc User's Guide}}

\vspace{4cm}

{\Huge {\sc Analyze-MD}}

\vspace{0.5 cm}
{\Large Version 0.1.3} 

\vspace{0.5 cm}

{\Large {\it - A program to analyze Molecular Dynamics trajectories -
}}


\vspace{1cm}

{\Large {\it March 12, 2008} }

\vspace{3cm}

{\Large Pablo Ordej\'on}

\vspace{5pt}

{\it Centre d'Investigaci\'o en Nanoci\`encia i Nanotecnologia (CSIC-ICN)
Campus de la UAB, 08193 Bellaterra, Barcelona, Spain}

\vspace{2pt}
{\bf pablo.ordejon@cin2.es}
\vspace{7mm}
\end{center}

\end{titlepage}

% END TITLE PAGE --------------------------------------------------------------

\tableofcontents

\newpage



\section{INTRODUCTION}

This {\sc analyze-MD} program is a utility for post-processing
Molecular Dynamics (MD) trajectories obtained from 
other simulation codes.
It is included among the utilities distributed with {\sc Siesta},
although it could be used for the analysis of MD trajectories
any other simulation code (provided that the data
from the simulation are prepared in the appropriate format).


{\sc analyze-MD} computes statistical averages of several
physical quantities from the MD trajectories provided
by the user. So far, these are the quantities which
are computed by {\sc analyze-MD}:

\begin{itemize}

\item{\bf Pair Distribution Functions}
\item{\bf Velocity-Velocity Autocorrelation Functions}
\item{\bf Atomic Mean Square Displacements}
\item{\bf Vibrational Power Spectrum}

\end{itemize}


Some of the subroutines and modules used by {\sc analyze-MD}
are taken from {\sc Siesta}, sometimes with slight
modifications. These are:

dismin, ddot, ranger, reccel, indarr, negihb, paste,
die, volcel, chemical, periodic\_table, readcel.

and the subdirectory fdf.

The routine to compute the time-correlation functions
uses as core algorithm for the time averages
a modified and corrected version of the
one published in Allen-Tildesley, ``Computer Simulation
of Liquids", Oxford Science Publications.


\section{BASIC EQUATIONS}

This section provides the basic equations for the
physical properties which {\sc analyze-MD} computes
from the MD trajectories. The reader is encouraged
to go to the especialized literature on MD for more 
details.
Many of these properties are time averages
of some physical quantity $f(\tau)$ along the simulation steps
$\tau$ of the MD run (assumed to go from 1 to $\tau_{max}$), 
which we define as:
%
\begin{equation}
 \langle f \rangle = {1 \over \tau_{max}} \sum_{\tau_0=1}^{\tau_{max}}
f(\tau_0).
\end{equation}
%
Time correlation functions are defined in the same
way as time-averages:
%
\begin{equation}
C_{fg} =  \langle f(\tau) g(0) \rangle = 
{1 \over \tau_{max}} \sum_{\tau_0=1}^{\tau_{max}}
f(\tau_0+\tau) g(\tau_0).
\label{eq:timecor}
\end{equation}
%
We will denote time averages with brackets $\langle ... \rangle$
throughout this document.





\subsection{Pair Distribution Functions}

{\sc analyze-MD} computes several pair distribution 
functions, for the different pairs of chemical species
present in the simulation. In particular:


\begin{itemize}

\item{Average Number of Neighbours}

For chemical species $A$, the Average Number of Neighbours  (ANN)
of species $B$ at distance $r$ is defined as
the average number of atoms of species $B$ which
are between $r$ and $r+\delta r$ if there is
an atom of species $A$ at the origin, divided by $\delta r$:
%
\begin{equation}
n_{A B} (r) = {1 \over \delta r}   \left\langle
{1 \over N_A} \sum_{i\in\{A\}}  \sum_{j\in\{B\}\ne i}
\delta(r - |\bf{r}_{ij}|)    \right\rangle
\label{eq:ann}
\end{equation}
%
in such a way that the radial integral of $n_{A B} (r)$
from $r=0$ to $r_0$ will give $N_{A B} (r_0)$,
the average number
of $B$ atoms in a sphere of radius $r_{max}$ around
an atom $A$:
%
\begin{equation}
N_{A B} (r_0) = \int_0^{r_0} n_{A B} (r) dr.
\end{equation}
%
Therefore, $n_{A B} (r)$ can be used in a straightforward
way to obtain the coordination number of a given species
in the system.

It is important to note that the ANN functions $n_{A B} (r)$ 
and $n_{B A} (r)$ are in general not equal, but proportional: 
%
\begin{equation}
N_A \times n_{A B} (r) = N_B \times n_{B A} (r) 
\end{equation}
%
\begin{equation}
N_A \times N_{A B} (r_0) = N_B \times N_{B A} (r_0) 
\end{equation}


\item{Radial Distribution Functions}

The Radial Distribution Function (RDF) for species $A$ and $B$
is defined as: 
\begin{equation}
  g_{AB}(r) = {V \over N_A N_B} \left\langle 
\sum_{i\in\{A\}} \sum_{j\in\{B\}\ne i} 
\delta({\bf r} - {\bf r}_{ij}) \right\rangle
\label{eq:rdf}
\end{equation}
%
(Note that, in an MD run with variable cell size, 
the volume $V$ corresponds to the
average volume during the simulation: $\langle V \rangle$).

The RDF and the ANN functions are, of course, closely
related.  $g_{AB}(r)$ is just $n_{AB}(r)$, normalized
to the corresponding ANN for an ideal gas with
the same density. Therefore, for liquids and gases,
 $g_{AB}(r)$ goes to 1 at large distances $r$.
Also, the RDFs are symmetric:
%
\begin{equation}
g_{A B} (r) = g_{B A} (r)
\end{equation}
%
in contrast to the ANNs.




\end{itemize}


\subsection{Velocity-Velocity Autocorrelation Functions}

The Velocity-Velocity Autocorrelation Function (VAC)
for species $A$ is defined as:
%
\begin{equation}
C_v^A (t) = {1 \over N_A} \sum_{i \in A} \left\langle {\bf v}_i(t) \cdot 
{\bf v}_i(0) \right\rangle
\label{eq:vva}
\end{equation}
%
From $C_v (t)$ several physical properties of interest can be
computed for each species. One of them is the Diffusion 
Coefficient:
%
\begin{equation}
D_A = {1\over 3} \int_0^\infty C_v^A (t) dt
\label{eq:diff1}
\end{equation}
%



\subsection{Atomic Mean Square Displacements}

The Atomic Mean Square Displacement (MSD) for species $A$ is
defined as:
%
\begin{equation}
\langle r^2_A \rangle (t) = {1 \over N_A} \sum_{i \in A} \left\langle \left|
{\bf r}_i(t) -
{\bf r}_i(0) \right|^2 \right\rangle
\label{eq:msd}
\end{equation}
%
The MSD is also related to the Diffusion Coefficient, through the
Einstein relation:
%
\begin{equation}
\langle r^2_A \rangle (t) = 6 D_A t + c_A
\end{equation}
%
At long times, $D_A$ can be extracted from $\langle r^2_A \rangle (t)$
by a least square fit to a straight line.
A {\bf rough} estimate (with error of ${\cal O}(1/t)$)
can also be obtained at very long times using:
%
\begin{equation}
D_A \approx {1 \over {6t}} \langle r^2_A \rangle (t),
\label{eq:diff2}
\end{equation}
%
although, of course, it is always better to do
a least-squares fit of  $\langle r^2_A \rangle (t)$
at the time range where it has reached a linear behaviour.

\subsection{Vibrational Power Spectrum}

The Power Spectrum is a Fourier analysis of the velocity-velocity
autocorrelation function, which is directly related with the
vibrational (phonon) density of states [see Sankey {\em et al.}, PRB 
{\bf 40}, 3979 (1989); Lee {\em et al.}, PRB {\bf 47}, 4893 (1993);
Kim {\em et al.}, PRB {\bf 52}, 14709 (1995)]. We define the Power
Spectrum for chemical species $A$ as:
%
\begin{equation}
g_A(\omega) = {1 \over \pi} \int_{-T_{max}}^{T_{max}} 
C_v^A(t) \cos(\omega t) W(t) dt
\label{eq:power}
\end{equation}
%
where, again, $T_{max}$ is the length of the simulation
(which is supposed to start at t=0), and $W(t)$ is
a window function used to dump the noise and
oscilations produced by the finite time lenght
of the simulation (at this point, the code uses
the Hamming window).
%see \begin{verbatim} 
%http://en.wikipedia.org/wiki/Window_function
%\end{verbatim}). 

This equation 
assumes that the correlation functions $C_v(t)$
are symmetric in time: $C_v(-t) = C_v(t)$, as follows
from their definition in Eqs. \ref{eq:timecor} and \ref{eq:vva}.

If the simulation is long enough, so that the system 
is truly equilibrated and each vibrational mode is 
excited according to the system temperature,
then the Power Spectrum is proportional to
the density of vibrational states [see  Lee {\em et al.}, PRB {\bf 47}, 
4893 (1993)].

Using our definition of the Power Spectrum in Eq. \ref{eq:power},
it is easy to see that the integral over frequencies is 
normalized to one:
%
\begin{equation}
\int_0^\infty g_A(\omega) d\omega = 1.
\end{equation}
%
Therefore, it represents the average density of vibrational
states per vibrational degree of freedom for each
chemical species. If there are $N_A$ atoms of species
$A$, the total vibrational density of states
of the system will be:
%
\begin{equation}
VDOS(\omega) = N_d \sum_A N_A g_A(w),
\end{equation}
%
$N_d$ being the number of dimensions in space
where the atoms are allowed to move (usually 3).







\section{HOW TO COMPILE AND RUN THE PROGRAM}

In this section we give all the steps required to install
and run the program. 
We assume that you use UNIX, that you will install {\sc analyze-MD} in 
$\sim$/analyze-MD/Src.
The commands that you must type are in {\tt typewriter} font, 
and {\tt \#} indicates the prompt.



\subsection{Compilation}

  \begin{itemize}

   \item Change to the directory where the sources of {\sc analyze-MD} are:

         {\tt \# cd $\sim$/analyze-MD/Src}

   \item Create a {\tt arch.make} file
         to define the Fortran 90 compiler in your
         system, and the compilation flags. For  {\sc Siesta}
         users, you can just use the same ones which appear
         in the  {\sc Siesta} arch.make file.

         You can find  sample arch.make files in the {\tt Src/Sys}
         directory.

   \item Type make:
         {\tt \# make}

   \item The program should compile and an executable file called
         analyze-MD should be created.

  \end{itemize}

\subsection{Running the code}

 {\sc analyze-MD} needs some information about your 
 physical system, and the data from your MD simulation.
 These are provided in two input files described in Section \ref{cap:input}

 The first input file contains basic information 
about the physical system and the parameters of the run
(as described in Section \ref{cap:maininput}).
We will assume that it's name is {\it analyze-MD}.fdf

The second one is a file containing the data (positions/velocities)
from the MD run (as described in Section \ref{cap:mdinput}).
We will assume that it's name is {\it SystemLabel}.MD

To run {\sc Analyze-MD}, assuming you are in a directory
which contains the two input files, and from where
you can locate the executable file {\tt analyze-MD},
just type:

 {\tt \#  analyze-MD < {\it analyze-MD}.fdf } 
%

\noindent
Some information on what's going on will
be dumped to standard output (which, of course 
you can redirect to a file if you wish).
And the results of the calculation with all
the physical quantities of interest will be dumped
to files described in Section \ref{cap:output}.

\section{INPUT FILES}
\label{cap:input} 

You need just two input files to run {\sc Analyze-MD}.
The first one is the main input file, which contains all the parameters
needed to define your system and to specifiy the details
of the run. The second one is a file with the atomic coordinates
and velocities (and also the cell vectors and their time
derivatives, for runs with variable cell), for your
MD run. This section describes the variables, format
and contents of these two data files.


\subsection{Main Input File}
\label{cap:maininput}

The main input data file is written in an special format called FDF, developed
by Alberto Garc\'{\i}a and Jos\'e M. Soler (see {\sc Siesta} USER'S GUIDE).
It contains all the parameters needed to specify the details
of the run.
Here is a description of the variables that you can define in your 
{\sc analyze-MD} input file,
with their data types and default values.

If you are using {\sc analyze-MD} to analyze the
results of a {\sc Siesta} run, many of the input variables 
required are the same as those defined in your
{\sc Siesta} run. These are the variables described
in Section \ref{cap:gsd}.
Therefore, you can just 'cut and paste'
from the {\sc Siesta} input file, or include that
file in your  {\sc analyze-MD} input file (using
the {\tt \%include} command  from the FDF language.
That will assure that the data introduced correspond
exactly to those used in the {\sc Siesta} run.


\vspace{5pt}
\subsubsection{General system descriptors}
\label{cap:gsd}

These input variables contain information to
describe the physical system. Their names
and meaning are the same as those of the
{\sc Siesta} code.  For the analysis of
results from a {\sc Siesta} run, they should
have the same values as in the  {\sc Siesta} 
input file; in that case, you may just
include your {\sc Siesta} input file in your
{\sc Analyze-MD} input using the FDF {\tt \%include}
command, to make sure all the variables have
the right value. 

\begin{description}
\itemsep 10pt
\parsep 0pt

\item[{\bf SystemLabel}] ({\it string}): 
A {\bf single} word (max. 20 characters {\bf without blanks})
containing a nickname of the system, used to name output files. 

{\it Default value:} siesta

\item[{\bf NumberOfAtoms}] ({\it integer}):
Number of atoms in the simulation.

{\it Use:} This variable is mandaroty. 

{\it Default value:} There is no default. You must supply this variable.

\item[{\bf NumberOfSpecies}] ({\it integer}):
Number of different atomic species in the simulation.
In a {\sc Siesta} run, atoms of the same species, but with a different
pseudopotential or basis set are counted as different species.

{\it Use:} This variable is mandaroty. 

{\it Default value:} There is no default. You must supply this variable.


\item[{\bf ChemicalSpeciesLabel}] ({\it data block}):
It specifies the different chemical species\index{species} that are present,
assigning them a number for further identification.
See the {\sc Siesta} User Guide for details on format.
The different atomic species are recognized by the given atomic number.
The atomic symbol is just a label, which may not correspond
to the actual atomic symbol of that element.

\begin{verbatim}
         %block Chemical_Species_label
            1   6   C
            2  14   Si
            3  14   Si_surface
         %endblock Chemical_Species_label
\end{verbatim}

The first number in a line is the species number, it is followed by the
atomic number, and then by the desired label. You can distinguish
several atoms with the same atomic number with different
species label, which is useful for different purposes.
For instance, you may want to analyze separately
different atoms of the same atomic species. Also, you may
have used different parameters in the simulation
(force constants, pseudopotentials, basis sets, etc) in
for describing different atoms of the same species.


{\it Use:} This block is mandatory.

{\it Default:} There is no default. You must supply this block.




\item[{\bf LatticeConstant}] ({\it real length}):
Lattice constant. This is just to define the scale of the lattice vectors.

{\it Use:} This variable is mandatory if {\bf AMD.VariableCell} 
{\tt =".false."} It is not used if {\bf AMD.VariableCell} 
{\tt =".true."}

{\it Default value:} There is no default. You must supply this variable
if {\bf AMD.VariableCell} 
{\tt =".false."}
{\bf WARNING for {\sc Siesta} users:} this variable is not
mandatory in {\sc Siesta}; however, it must be supplied to run
{\sc Analyze-MD}. If you use the input file of {\sc Siesta},
make sure that you include it in the {\sc Analyze-MD} input.

\item[{\bf LatticeParameters}] ({\it data block}):
Crystallographic way of specifying the lattice vectors, by giving
six real numbers: the three vector modules, $a$, $b$, and $c$, and
the three angles $\alpha$ (angle between $\vec b$ and $\vec c$),
$\beta$, and $\gamma$. The three modules are in units of
{\bf LatticeConstant}, the three angles are in degrees.

{\it Default value:}
{\tt
\begin{verbatim}
           1.0   1.0   1.0    90.   90.  90.
\end{verbatim}
}
\noindent
(see the following)

\item[{\bf LatticeVectors}] ({\it data block}):
The cell vectors are read in units of the lattice constant defined above.
They are read as a matrix {\tt CELL(ixyz,ivector)}, each vector being
one line.

{\it Default value:}
{\tt
\begin{verbatim}
            1.0    0.0    0.0 
            0.0    1.0    0.0 
            0.0    0.0    1.0 
\end{verbatim}
}
\noindent
If the {\bf LatticeConstant} default is used, the default of
{\bf LatticeVectors} is still diagonal but not necessarily cubic.
\item[{\bf AtomicCoordinatesFormat}] ({\it string}):
\index{AtomicCoordinatesFormat@{\bf AtomicCoordinatesFormat}}
Character string to specify the format of the atomic positions in
input. These can be expressed in four forms:
\begin{itemize}
\item {\tt Bohr} or {\tt NotScaledCartesianBohr} (atomic positions
are given directly in Bohr, in cartesian coordinates)
\item {\tt Ang} or {\tt NotScaledCartesianAng} (atomic positions
are given directly in {\AA}ngstr\"om, in cartesian coordinates)
\item {\tt ScaledCartesian} (atomic positions are given
in cartesian coordinates, in units of the lattice constant)
\item {\tt Fractional} or {\tt ScaledByLatticeVectors} (atomic positions
are given referred to the lattice vectors)
\end{itemize}

{\it Default value:} {\tt NotScaledCartesianBohr}


\item[{\bf AtomicCoordinatesAndAtomicSpecies}] ({\it data block}):
The reading is done this way:
\begin{verbatim}
       From ia = 1 to natoms
            read: xa(ix,ia), isa(ia)
\end{verbatim}
where {\tt xa(ix,ia)} is the {\tt ix} coordinate of atom
{\tt iai}, and {\tt isa(ia)} is the species index of atom {\tt ia}.

\item[{\bf MD.LengthTimeStep}]({\it real time}):
Length of the time step of the MD simulation.

{\it Default value:} {No default value; Must be specified.
(Note that this is in contrast with {\sc Siesta}, where
the MD.LengthTimeStep variable has a default of 1.0 fs).}


\end{description}



\vspace{5pt}
\subsubsection{Variables specific to {\sc analyze-MD}}

\begin{description}
\itemsep 10pt
\parsep 0pt

\item[{\bf AMD.VariableCell}] ({\it logical}):
This flag specifies if the MD run corresponds to
one in which the cell vectors were kept fixed,
or if they changed (as in Parrinello-Rahman
dynamics).
{\bf Attention!:} It is important to define this variable correctly,
since the reading of the MD coordinates and velocities
file depends critically on this. Program crashes
may likely be due to a wrong choice of this
variable.

{\it Default value:} {\tt .false.}

\item[{\bf AMD.InitialTimeStep}]({\it integer}):
Index of the initial time step that will be used for
the analysis of the MD data.
Data in MD file prior to this time step will
be discarded and not used in the data
analysis.

{\it Default:} {\tt 1}

\item[{\bf AMD.FinalTimeStep}]({\it integer}):
Index of the final time step that will be used for
the analysis of the MD data.
Data in MD file after this time step will
be discarded and not used in the data
analysis.  If AMD.FinalTimeStep is larger
than the actual number of time steps stored
in the MD file, the program will stop with
a message.

{\it Default:} {\tt Last time steps on file.}

\item[{\bf AMD.TimeSkipInterval}]({\it integer}):
The analysis of the MD data will not be done
for all the time steps in the MD file, but
selecting time steps at intervals
of AMD.TimeSkipInterval.
For the calculation of
the Pair Distribution Functions, this means
that only one of every AMD.TimeSkipInterval
time points will be used in the average.
For the calculation of the time correlation
functions (Velocity-Velocity Autocorrelation
and Atomic Mean Displacements),
this means that only one of every AMD.TimeSkipInterval
time points will be used as starting point $\tau_0$
in the time averages (Eq. \ref{eq:timecor}).
If AMD.TimeSkipInterval=1, all time steps are used
in the analysis.

{\it Default:} {\tt 1}

\item[{\bf AMD.RDF.Range}]({\it real length}):
Maximum radius for the calculation of the Pair Correlation Functions
(Radial Distribution Function and Average Number of Neighbours).

{\it Default:} {\tt  12.0  {\AA}ngstr\"om}

\item[{\bf AMD.RDF.NRbins}]({\it integer}):
Number of radial points in the calculation of the
 Pair Correlation Functions
(Radial Distribution Function and Average Number of Neighbours).

{\it Default:} {\tt 200}

\item[{\bf AMD.RDF.Sigma}]({\it real length}):
Value of the standard deviation $\sigma$ 
of a Gaussian used for broadening the
calculated Pair Correlation Functions.
The raw data are convoluted with a Gaussian 
$g(r) = {1 \over {\sigma \sqrt{2\pi}}} \exp{-(r^2 / 2 \sigma^2)}$
to improve smoothness.

{\it Default:} {\tt 0.1   {\AA}ngstr\"om}

\item[{\bf AMD.RDF.PolynomialSmoothing}]({\it logical}):
Flag to define if a smoothing of the raw data using
a polynomial interpolation is done. 
If {\tt .true.}, each raw data points is replaced
by a least-squares third-degree polynomial fit
to five neighbouring raw data points.

{\it Default:} {\tt .false.}


\item[{\bf AMD.TIMEFUNCS.NTbins}]({\it integer}):
Number of time steps for which the  
 time-dependent functions (such as the Velocity-Velocity
Autocorrelation or the Mean Sqare Displacements) will be calculated.
Therefore, these functions will be obtained
for a total real time defined by the
product AMD.CORRFUNC.NTbins $\times$ 
MD.LenghTimeStep.

{\it Default:} {\tt Full length of the simulation,
or length of the time window for analysis 
(AMD.FinalTimeStep - AMD.InitialTimeStep) if
this is smaller.}

\item[{\bf AMD.POWER.Nfreq}]({\it integer}):
Number of frequency values for which the  
Power Spectrum will be computed.

{\it Default:} {\tt 250}.

\item[{\bf AMD.POWER.wmin}]({\it real energy}):
Minimum frequency for which the Power Spectrum
is computed.

{\it Default:} {\tt 0.0 cm-1}.

\item[{\bf AMD.POWER.wmax}]({\it real energy}):
Maximum frequency for which the Power Spectrum
is computed.

{\it Default:} {\tt 1000.0 cm-1}.


\end{description}


\subsection{The MD data file}
\label{cap:mdinput}

The MD data file contains the information about the
coordinates and velocities of each atom in the 
simuation on each time step. If the MD run was
a variable cell one (i.e., the cell vectors of the 
simulation box change with time, as for instance
in Parrinello-Rahman dimanics), then the file
also contains the cell vectors and their time
derivatives at each MD step.

The format of the file is the same one used by
{\sc Siesta} to produce the {\it SystemLabel}.MD
file, which contains precisely the same information.
Therefore, if you are using {\sc Analyze-MD} to
analyze the results of a {\sc Siesta} run, you
can just use the {\it SystemLabel}.MD file
produced by {\sc Siesta}. If you are using
another simulation program, you have to build
your MD file in the way defined in the next
Fortran code:

\begin{itemize}
\item{Run with fixed cell vectors}
\begin{verbatim}
      integer NA, NTIME
      real*8 x(3,NA),v(3,NA)

      do it=1,NTIME
      ...    ! determine positions and velocities at time it
      write(iunit) it,x,v
      enddo
\end{verbatim}
\item{Run with variable cell vectors}
\begin{verbatim}
      integer NA, NTIME
      real*8 x(3,NA),v(3,NA),cell(3,3),vcell(3,3)

      do it=1,NTIME
      ...!    determine positions and velocities at time it
      ...!    determine cell vectors and velocities at time it
      write(iunit) it,x,v
      write(iunit) cell,vcell
      enddo
\end{verbatim}

\end{itemize}
where: 
\begin{verbatim}
      NA         = Number of atoms
      NTIME      = Number of time steps in the simulation
      xa(3,NA)   = Atomic coordinates at time it
      va(3,NA)   = Atomic velocities at time it
      cell(3,3)  = Cell vectors at time it
                   cell(i,j) = component a of lattice vector j
      vcell(3,3) = Cell velocities at time it
\end{verbatim}

\noindent
{\bf IMPORTANT!:} Units for these quantities are
assumed to be Bohr for distances and Bohr/fs for  velocities.


\section{OUTPUT FILES}
\label{cap:output} 

{\sc Analyze.MD} will produce the following output files:

\begin{description}
\itemsep 10pt
\parsep 0pt

\item[{\bf {\it A-B}.ANN}]: 
File containing the Average Number of Neighbours $n_{A B} (r)$,
(defined by Eq. \ref{eq:ann}), for species {\it A} and {\it B}.
There is one such file for each pair of chemical species
present in the simulation.

First column is the radius (in {\AA}ngstr\"om).

Second column is $n_{A B} (r)$ (in atoms/{\AA}ngstr\"om).

\item[{\bf {\it A-B}.RDF}]: 
File containing the Radial Distribution function
$g_{A B} (r)$
(defined by Eq. \ref{eq:rdf}), for species {\it A} and {\it B}.
There is one such file for each pair of chemical species
present in the simulation.

First column is the radius (in {\AA}ngstr\"om).

Second column is $g_{A B} (r)$ (adimensional quantity).

\item[{\bf {\it A}.VVA}]: 
File containing the Velocity-Velocity Autocorrelation
Function
$C_v^A(t)$ 
(defined by Eq. \ref{eq:vva}), for species {\it A}.
There is one such file for each chemical species
present in the simulation.
It also contains the Diffusion coefficient $D_A$
obtained from the Velocity-Velocity Autocorrelation
Function through Eq. \ref{eq:diff1}

First column is time (in fs).

Second column is $C_v^A$ (in ({\AA}ngstr\"om/fs)$^2$).

Third column is $D_A$ (in {\AA}ngstr\"om$^2$/fs)

{\bf IMPORTANT!:} The program corrects the velocity
of the atoms read from the impuf file, so that
the center of mass velocity is zero. This
cancels errors in the analysis due to motion
of the center of mass in the MD trajectories.


\item[{\bf {\it A}.MSD}]: 
File containing the atomic Mean Square Displacement
$\langle r^2_A \rangle (t)$ 
(defined by Eq. \ref{eq:msd}), for species {\it A}.
There is one such file for each chemical species
present in the simulation.
It also contains the Diffusion coefficient $D_A$
estimated from the Mean Square Displacement
through Eq. \ref{eq:diff2}

First column is time (in fs).

Second column is $\langle r^2_A \rangle (t)$ (in {\AA}ngstr\"om$^2$).

Third column is $D_A$ (in {\AA}ngstr\"om$^2$/fs)

{\bf IMPORTANT!:} The program corrects the positions
of the atoms read from the impuf file, so that
the center of mass position is fixed during
the dynamics. This
cancels errors in the analysis due to motion
of the center of mass in the MD trajectories.

\item[{\bf {\it A}.VDOS}]: 
File containing the Power Spectrum (or vibrational
density of states) $g_A(\omega)$ 
defined by Eq. \ref{eq:power}, for species {\it A}.
There is one such file for each chemical species
present in the simulation.

First column is frequency (in cm$^{-1}$).

Second column is $g(\omega)$ (in states/cm$^{-1}$).
 
\end{description}

\end{document}
