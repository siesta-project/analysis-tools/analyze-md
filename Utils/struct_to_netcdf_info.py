#!/usr/bin/env python

#
# Example of python script for processing of MD netcdf files.
#
# Addition of extra information to a raw MD netCDF file
#
# The information should come from a Structure object

from Numeric import *
from Siesta.ASEInterface import *
from Scientific.IO.NetCDF import *
import sys

#
fname=sys.argv[1]
from soup_61atoms import atoms    ### Change this 

file = NetCDFFile(fname, "a")   # For appending

## Create some global attribute using a constant
setattr(file, 'versionNumber', 1)
#
NATOMS = file.dimensions['atom']

species_list = atoms.GetSpecies()

NSPECIES = len(species_list)
species = file.createDimension('species', NSPECIES)
#
speciesDims = ( 'species' ,)
label_len = file.createDimension('label_len', 20)
species_label = file.createVariable('species_label','c', ('species' ,'label_len'))
#
species_number = file.createVariable('species_number','i', ('atom',) )
mass = file.createVariable('mass','d', ('atom',))

for i in arange(NSPECIES):
     length=len(species_list[i])
     species_label[i,:length] = species_list[i]

mass[:] = atoms.GetMasses()
species_number[:] = atoms.GetSpeciesNumbers()
     
file.close()
