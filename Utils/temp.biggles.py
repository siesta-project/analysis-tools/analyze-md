#!/usr/bin/env python

#
# Example of python script for processing of MD netcdf files.
#
# Plot of temperature

from Numeric import *
from Scientific.IO.NetCDF import *
import biggles
import sys
import MLab

#
fname=sys.argv[1]
n_equil=int(sys.argv[2])

print "File, n_equil:", fname, n_equil

of = NetCDFFile(fname)
print "Dimensions:", of.dimensions
na=of.dimensions['atom']    # Number of atoms

temperature=of.variables['temp'][n_equil:]
nsteps = len(temperature)
step = arange(0,nsteps)
print nsteps, len(step)

average= MLab.mean(temperature)
print "Average temperature:", average

#-------- Moving average  -- a bit crude
lag = 100
#print nsteps/lag
moving_average = arrayrange(0,1+nsteps/lag,1,'f')

#print "Length of moving average: ", len(moving_average)

for i in range(1,len(moving_average)):
#    print (i-1)*lag, i*lag
    moving_average[i] = MLab.mean(temperature[(i-1)*lag:i*lag-1])

moving_average[0] = moving_average[1]
step_ma = arange(0,len(moving_average))

p=biggles.FramedPlot(title=fname)
p.add(biggles.Curve(step,temperature,color="red"))
p.add(biggles.Curve(step_ma*lag,moving_average,color="blue"))
p.show()
#p.save_as_img( "gif", 400, 400, fname+".temp.gif" )
