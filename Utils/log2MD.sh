#!/bin/sh
#
rm -f tmp.awk
cat << 'EndOfAWKFile' > tmp.awk    # Note special quotes to prevent expansion
#  
# md.awk:  Processes a LOG file from a MD simulation and outputs
#          the information needed for the Siesta-compatible MD file
#          This output has to be processed by mdasc2bin
#
#    Typical chain in the shell:
#
#    [ -f MD ] && rm -f MD              # Remove MD if it exists
#    awk -f md.awk LOG | mdasc2bin      # Will produce binary MD
#    

/MDstep/ {$1=""; print}
/ixv/ {  $1 = ""; i= $2; $2 = ""; print $0, "\t", " # ", i}

EndOfAWKFile
#
#
if [ "$#" != "1" ] 
then
   echo "Usage: $0 LogFile"
   exit 1
fi
#
logfile=$1
if [ ! -r $logfile ] 
then
   echo "Cannot find logfile $logfile"
   exit 1
fi
#
[ -f MD ] && rm -f MD                    # Remove MD if it exists
awk -f tmp.awk $logfile | mdasc2bin      # Will produce binary MD
