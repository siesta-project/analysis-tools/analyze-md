#!/bin/sh
#
rm -f tmp.awk
cat << 'EndOfAWKFile' > tmp.awk    # Note special quotes to prevent expansion
#  
# md.awk:  Processes a LOG file from a MD simulation and outputs
#          the information needed for the Siesta-compatible MD netCDF file
#          This output has to be processed by mdasc2cdf
#
#    Typical chain in the shell:
#
#    [ -f MD ] && rm -f MD              # Remove MD if it exists
#    awk -f md.awk LOG | mdasc2bin      # Will produce binary MD
#    

/MDstep/ {$1=""; print}
/ixv/ {  $1 = ""; i= $2; $2 = ""; print $0, "\t", " # ", i}
/Center/ { print $NF}
/Temperature/ { print $2}
EndOfAWKFile
#
#
if [ "$#" != "1" ] 
then
   echo "Usage: $0 LogFile"
   exit 1
fi
#
logfile=$1
if [ ! -r $logfile ] 
then
   echo "Cannot find logfile $logfile"
   exit 1
fi
#
[ -f MD.nc ] && rm -f MD.nc                    # Remove MD.nc if it exists
awk -f tmp.awk $logfile | mdasc2cdf      # Will produce MD.nc
