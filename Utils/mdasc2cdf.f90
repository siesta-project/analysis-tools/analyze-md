!--------------------------------------------------------

program mdasc2cdf
! convert ASCII MDlog information to netCDF form
!
! Reads ASCII MDlog info from standard input
! The units of MD.nc are bohr for x and bohr/femtosecond for v
!
! The units in the ASCII MDlog info  must be Ang for x and Ang/time_unit for v,
! where time_unit is based on (eV, Ang, amu) (= 10.1806 femtoseconds)
! (as produced by the MD scripts in Python)
!

use netcdf

implicit none

integer, parameter :: dp = selected_real_kind(14,100)

integer :: istep, na, nrec, istep0
integer :: iostat, ia

! Default units are eV, Ang, amu
! In them:

real(dp), parameter :: bohr  = 0.5292_dp
real(dp), parameter :: femtosecond = 1.0_dp / 10.1806_dp
real(dp), parameter :: siesta_v_unit = bohr / femtosecond


real(dp), dimension (:,:), allocatable :: xa, va
real(dp)                               :: temp
!!!real(dp)                   ::  eks, ekin, edrift
real(dp)                      :: center_of_mass_v_sq, cm_v

integer iret
integer  :: ncid 
integer  :: xyz_id, atom_id, step_id
integer  :: xa_id, va_id
integer  :: temp_id , cm_v_id
!!!integer  :: eks_id, etot_id, edrift_id

integer        :: step_no
integer        :: atom_no

read(*,*) istep0, na
allocate (xa(3,na), va(3,na))

iret = nf90_create("MD.nc",NF90_SHARE+NF90_WRITE,ncid)
call check(iret)

iret = nf90_def_dim(ncid,'xyz',3,xyz_id)
call check(iret)
iret = nf90_def_dim(ncid,'atom',na,atom_id)
call check(iret)
iret = nf90_def_dim(ncid,'step',NF90_UNLIMITED,step_id)
call check(iret)

!iret = nf90_def_var(ncid,'isa',nf90_int,(/atom_id/),isa_id)
!call check(iret)
!iret = nf90_put_att(ncid,isa_id,'Description',"Species index")
!call check(iret)
!iret = nf90_def_var(ncid,'iza',nf90_int,(/atom_id/),iza_id)
!call check(iret)
!iret = nf90_put_att(ncid,iza_id,'Description',"Atomic number")
!call check(iret)

iret = nf90_def_var(ncid,'temp',nf90_double,(/step_id/),temp_id)
call check(iret)
iret = nf90_put_att(ncid,temp_id,'Description',"Temperature in K")
call check(iret)

!!$iret = nf90_def_var(ncid,'eks',nf90_double,(/step_id/),eks_id)
!!$call check(iret)
!!$iret = nf90_put_att(ncid,eks_id,'Description',"Kohn-Sham energy in eV ")
!!$call check(iret)
!!$
!!$iret = nf90_def_var(ncid,'etot',nf90_double,(/step_id/),etot_id)
!!$call check(iret)
!!$iret = nf90_put_att(ncid,etot_id,'Description',"Total energy in eV")
!!$call check(iret)
!!$
!!$iret = nf90_def_var(ncid,'edrift',nf90_double,(/step_id/),edrift_id)
!!$call check(iret)
!!$iret = nf90_put_att(ncid,edrift_id,'Description',"Drift in energy in eV")
!!$call check(iret)

iret = nf90_def_var(ncid,'cm_v',nf90_double,(/step_id/),cm_v_id)
call check(iret)
iret = nf90_put_att(ncid,cm_v_id,'Description',"CM velocity (bohr/fs)")
call check(iret)

iret = nf90_def_var(ncid,'xa',nf90_double,(/xyz_id,atom_id,step_id/),xa_id)
call check(iret)
iret = nf90_put_att(ncid,xa_id,'Description', &
      "Atomic coordinates in cartesian bohr: xyz, ia, step")
call check(iret)

iret = nf90_def_var(ncid,'va',nf90_double,(/xyz_id,atom_id,step_id/),va_id)
call check(iret)
iret = nf90_put_att(ncid,va_id,'Description', &
      "Atomic velocities in cartesian bohr/femtosecond: xyz, ia, step")
call check(iret)

iret = nf90_enddef(ncid)
call check(iret)
!
!      Put unchanging stuff
!
!       iret = nf90_put_var(ncid, isa_id, isa(1:na), start = (/ 1 /) )
!       call check(iret)
!       iret = nf90_put_var(ncid, iza_id, iza(1:na), start = (/ 1 /) )
!       call check(iret)

! Now append the interesting information

nrec = 0
step_no = 0
do
   if (nrec == 0) then
      istep = istep0
   else
      read(unit=*,fmt=*,iostat=iostat) istep, na
      if (iostat /= 0) then
         print *, "iostat: ", iostat
         exit
      endif
   endif

   do ia = 1, na
      read(unit=*,fmt=*,iostat=iostat) xa(:,ia) , va(:,ia)
      if (iostat /= 0) then
         print *, "iostat xa, va: ", iostat
         exit
      endif
   enddo
   xa = xa/bohr
   va = va/siesta_v_unit
   read(unit=*,fmt=*,iostat=iostat) center_of_mass_v_sq
      if (iostat /= 0) then
         print *, "iostat center_of_mass: ", iostat
         exit
      endif
      read(unit=*,fmt=*,iostat=iostat) temp
      if (iostat /= 0) then
         print *, "iostat temp: ", iostat
         exit
      endif
!!$      read(unit=*,fmt=*,iostat=iostat) ekin, eks, edrift
!!$      if (iostat /= 0) then
!!$         print *, "iostat ekin, eks, edrift: ", iostat
!!$         exit
!!$      endif

   ! By convention, the first step in an MD file is number 1
   ! In case we are reading concatenated LOG files, just increment step_no

   step_no = step_no + 1

! put values
    iret = nf90_put_var(ncid, temp_id, temp, start = (/ step_no /) )
    call check(iret)

!!$    iret = nf90_put_var(ncid, eks_id, eks, start = (/ step_no /) )
!!$    call check(iret)
!!$
!!$    iret = nf90_put_var(ncid, etot_id, ekin+eks, start = (/ step_no /) )
!!$    call check(iret)
!!$
!!$    iret = nf90_put_var(ncid, edrift_id, edrift, start = (/ step_no /) )
!!$    call check(iret)

    cm_v = sqrt(center_of_mass_v_sq) / siesta_v_unit
    iret = nf90_put_var(ncid, cm_v_id, cm_v, start = (/ step_no /) )
    call check(iret)

    iret = nf90_put_var(ncid, xa_id, xa, start = (/1, 1, step_no /), &
                        count = (/3, na, 1 /) )
       call check(iret)

    iret = nf90_put_var(ncid, va_id, va, start = (/1, 1, step_no /), &
                        count = (/3, na, 1 /) )
       call check(iret)

!!!!   write(1) step_no , xa , va

   nrec = nrec + 1

enddo

write(0,*) "Processed ", nrec, " steps."

    iret = nf90_close(ncid)
    call check(iret)

CONTAINS

subroutine check(code)
use netcdf, only: nf90_noerr, nf90_strerror
integer, intent(in) :: code
if (code /= nf90_noerr) then
  write(0,*) "netCDF error: " // NF90_STRERROR(code)
  stop
endif
end subroutine check

end program mdasc2cdf

       
