! mdasc2bin
! convert binary MD file to ASCII
!
! Reads bin MD from  file MD
! Writes ASCII MD to file "MDFMT", which must not exist.
! The units of MD are bohr for x and bohr/femtosecond for v
! The units of MDFMT are Ang for x and Ang/time_unit for v,
! where time_unit is based on (eV, Ang, amu) (= 10.1806 femtoseconds)
!
program mdbin2asc

integer, parameter :: dp = selected_real_kind(14,100)

! Default units are eV, Ang, amu
! In them:

real(dp), parameter :: bohr  = 0.5292_dp
real(dp), parameter :: femtosecond = 1.0_dp / 10.1806_dp
real(dp), parameter :: siesta_v_unit = bohr / femtosecond

integer :: istep, na, nrec
integer :: iostat, ia

real(dp), dimension(:,:), allocatable  :: xa, va

open(unit=1,file="MD",form="unformatted", status="old")
open(unit=2,file="MDFMT",form="formatted", status="new")

write(*,*) "Enter number of atoms"
read(*,*) na

allocate (xa(3,na), va(3,na))

nrec = 0
do
   read(1,iostat=iostat) istep, xa, va
   if (iostat /= 0) exit
   write(unit=2,fmt="(2i8)") istep, na
   ! Convert to simulation units before writing
   do ia = 1, na
      write(unit=2,fmt="(3f13.6,3x,3f13.6,a,i8)") xa(:,ia)*bohr, va(:,ia)*siesta_v_unit, " # ", ia
   enddo
   nrec = nrec + 1
enddo

write(0,*) "Processed ", nrec, " steps."

end program mdbin2asc


