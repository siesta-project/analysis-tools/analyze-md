!--------------------------------------------------------

program mdcdf2bin

! convert MD netCDF info to MD form
!
! The units of MD.nc are bohr for x and bohr/femtosecond for v
!
use netcdf

implicit none

integer, parameter :: dp = selected_real_kind(14,100)

integer :: istep, na, nrec

real(dp), dimension (:,:), allocatable :: xa, va
real(dp)                               :: temp

integer iret
integer  :: ncid 
integer  :: xyz_id, atom_id, step_id
integer  :: xa_id, va_id

integer        :: step_no, nsteps, step1, step2

open(1,file="MD",form="unformatted", status="replace")

iret = nf90_open("MD.nc",NF90_NOWRITE,ncid)
call check(iret)
call check(  nf90_inq_dimid(ncid,'atom',atom_id) )
call check (  nf90_inquire_dimension(ncid,atom_id,len=na) )

call check(  nf90_inq_dimid(ncid,'step',step_id) )
call check (  nf90_inquire_dimension(ncid,step_id,len=nsteps) )

allocate (xa(3,na), va(3,na))

call check( nf90_inq_varid(ncid,'xa',xa_id) )
call check( nf90_inq_varid(ncid,'va',va_id) )

write(*,*) "Total number of steps: ", nsteps
write(*,*) "Enter range to keep: "
read(*,*) step1, step2

if (step1 .lt. 0 .or. step2 .gt. nsteps) then
   stop "OUT OF RANGE"
endif

nrec = 0
istep = 0
!
do step_no = step1, step2

    iret = nf90_get_var(ncid, xa_id, xa, start = (/1, 1, step_no /), &
                        count = (/3, na, 1 /) ) 
    call check(iret)
    iret = nf90_get_var(ncid, va_id, va, start = (/1, 1, step_no /), &
                        count = (/3, na, 1 /) ) 
    call check(iret)

    istep = istep + 1
    write(1) istep , xa , va

    nrec = nrec + 1
enddo

write(0,*) "Processed ", nrec, " steps."

iret = nf90_close(ncid)
call check(iret)
close(1)

CONTAINS

subroutine check(code)
use netcdf, only: nf90_noerr, nf90_strerror
integer, intent(in) :: code
if (code /= nf90_noerr) then
  write(0,*) "netCDF error: " // NF90_STRERROR(code)
  stop
endif
end subroutine check

end program mdcdf2bin

       
