! trim_md
! convert binary MD file to ASCII
!
! Reads bin MD from  file MD
! Writes ASCII MD to file "MDFMT", which must not exist.
! The units of MD are bohr for x and bohr/femtosecond for v
! The units of MDFMT are Ang for x and Ang/time_unit for v,
! where time_unit is based on (eV, Ang, amu) (= 10.1806 femtoseconds)
!
program trim_md

integer, parameter :: dp = selected_real_kind(14,100)

integer :: istep, na, nrec, step1, step2, istep_trim
integer :: nrec_trim
integer :: iostat

real(dp), dimension(:,:), allocatable  :: xa, va

open(unit=1,file="MD",form="unformatted", status="old")
open(unit=2,file="MD_TRIM",form="unformatted", status="new")

write(*,*) "Enter number of atoms, and initial and final steps of section to keep:"
read(*,*) na, step1, step2

allocate (xa(3,na), va(3,na))

nrec = 0
nrec_trim = 0
istep_trim = 0
do
   read(1,iostat=iostat) istep, xa, va
   if (iostat /= 0) exit
   if ((istep .ge. step1) .and. (istep .le. step2)) then
      istep_trim = istep_trim + 1
      nrec_trim = nrec_trim + 1
      write(unit=2) istep_trim, xa, va
   endif
   nrec = nrec + 1
enddo

write(0,*) "Processed ", nrec, " steps."
write(0,*) "Kept ", nrec_trim, " steps."

end program trim_md



