! mdasc2bin
! convert ASCII MDlog information to binary
!
! Reads ASCII MDlog info from standard input
! Writes binary MD to file "MD", which must not exist.
! The units of MD are bohr for x and bohr/femtosecond for v
!
! The units in the ASCII MDlog info  must be Ang for x and Ang/time_unit for v,
! where time_unit is based on (eV, Ang, amu) (= 10.1806 femtoseconds)
! (as produced by the MD scripts in Python)
!
program mdasc2bin

integer, parameter :: dp = selected_real_kind(14,100)

integer :: istep, na, nrec, istep0
integer :: iostat, ia, standard_step_no

! Default units are eV, Ang, amu
! In them:

real(dp), parameter :: bohr  = 0.5292_dp
real(dp), parameter :: femtosecond = 1.0_dp / 10.1806_dp
real(dp), parameter :: siesta_v_unit = bohr / femtosecond

real(dp), dimension(:,:), allocatable  :: xa, va

open(unit=1,file="MD",form="unformatted", status="new")

read(*,*) istep0, na
allocate (xa(3,na), va(3,na))

nrec = 0
do
   if (nrec == 0) then
      istep = istep0
   else
      read(unit=*,fmt=*,iostat=iostat) istep, na
      if (iostat /= 0) exit
   endif
   do ia = 1, na
      read(*,*) xa(:,ia) , va(:,ia)
   enddo
   ! By convention, the first step in an MD file is number 1
   standard_step_no = istep - istep0 + 1
   ! Convert to Siesta MD units
   nrec = nrec + 1
   write(1) nrec , xa/bohr , va/siesta_v_unit
enddo

write(0,*) "Processed ", nrec, " steps."

end program mdasc2bin


