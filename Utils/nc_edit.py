#!/usr/bin/env python

#
# Example of python script for processing of MD netcdf files.
#
# Addition of extra information
#
# The information should come from a Structure object

from Numeric import *
from Scientific.IO.NetCDF import *
import sys

#
fname=sys.argv[1]

file = NetCDFFile(fname, "a")   # For appending


## Create some global attribute using a constant
setattr(file, 'versionNumber', 1)

## Create a global attribute using a variable
magicNum = 42
setattr(file, 'magicNumber', magicNum)
#
NATOMS = file.dimensions['atom']
species_list = ['As', 'Ga', 'O']
NSPECIES = len(species_list)
species = file.createDimension('species', NSPECIES)
#
speciesDims = ( 'species' ,)
mass = file.createVariable('mass','d', speciesDims)
label_len = file.createDimension('label_len', 20)
species_label = file.createVariable('species_label','c', ('species' ,'label_len'))
#
species_number = file.createVariable('species_number','i', ('atom',) )


for i in arange(NSPECIES):
     length=len(species_list[i])
     species_label[i,:length] = species_list[i]

for i in arange(NSPECIES):
     mass[i] = 3.54

for i in arange(NATOMS):
     species_number[i] = 2
     
file.close()
