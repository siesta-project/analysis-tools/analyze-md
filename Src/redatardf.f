      SUBROUTINE REDATARDF( NA, NS, SNAME, VARCEL,
     .                   IS, SYM, RANGE, NBIN, SIGMA, SMOOTHINT, CELL,
     .                   TIMEINI, TIMEFIN, IOR)

C **********************************************************************
C Read the data file to calculate the RDF from the SIESTA MD file.
C P. Ordejon, February 2008
C **********************************************************************

      USE FDF
      USE CHEMICAL

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: 
     .   NA, NS

      LOGICAL, INTENT(OUT) ::
     .   VARCEL, SMOOTHINT

      INTEGER, INTENT(OUT) :: 
     .   NBIN, IS(*), TIMEINI, TIMEFIN, IOR

      DOUBLE PRECISION, INTENT(OUT) ::
     .  RANGE, SIGMA, CELL(3,3)

      CHARACTER(len=60), INTENT(OUT) :: SNAME
      CHARACTER(len=10), INTENT(OUT) :: SYM(*)



C **** INPUT ***********************************************************
C **** OUTPUT **********************************************************
C **********************************************************************

C Internal Variables
      INTEGER NRBIN_DEFAULT, I, IA, IUNIT,NSP
      DOUBLE PRECISION SIGMA_DEFAULT, RANGE_DEFAULT, XA(3)
      CHARACTER(LEN=100) PASTE

      EXTERNAL PASTE


C Assign the SystemLabel (name of .MD file)
      SNAME = FDF_STRING('SystemLabel','siesta')


      WRITE(6,'(2/,a)')
     . ' Reading system data needed for calculation of RDF           '
      WRITE(6,'(2a)')
     . ' from input fdf file and from ',
     .  PASTE(SNAME,'.MD dynamics file')


C Variable cell run?
      VARCEL = FDF_BOOLEAN('AMD.VariableCell', .false. )
      write(6,'(/,a,l1)') 
     . ' Variable Cell run?:                              ',VARCEL
      if (VARCEL) then
       write(6,'(a)') ' Cell vectors will be read from MD file, and'
       write(6,'(a,/)') ' values from input fdf file will NOT be used!'
      else
       write(6,'(a,/)') ' Cell vectors will be read from input fdf file'
      endif

C Read AtomicCoordinatesAndAtomicSpecies block

      IF (.NOT.VARCEL) CALL READCEL(CELL)

      IF ( FDF_BLOCK('AtomicCoordinatesAndAtomicSpecies',IUNIT) )
     .     THEN
C XA: Dummy vector; values not used.
         DO IA = 1, NA 
            READ(IUNIT,*) (XA(I), I=1,3), IS(IA)
         ENDDO
      ELSE
         CALL DIE("REDATA: You must specify the atomic coordinates")
      ENDIF


C Read Atomic Species information

      CALL READ_CHEMICAL_TYPES
      NSP = NUMBER_OF_SPECIES()
      IF (NSP.NE.NS) stop 'Error: wrong number of species'

      write(6,'(/,a)') ' Reading Atomic Species'
C      write(6,'(/,a)') '    Atom    Species'
C      write(6,'(a)')   ' --------------------'
      DO IA = 1, NA
        SYM(IS(IA)) = SPECIES_LABEL (IS(IA))
C        write(6,'(i8,8x,a)') ia,sym(is(ia))
      ENDDO


C Read maximum radius for plotting RDF

      RANGE_DEFAULT = 12.0D0
      RANGE = FDF_PHYSICAL('AMD.RDF.RANGE',RANGE_DEFAULT,'Ang' )
      write(6,'(/,a,f5.2,a)') 
     . ' Range of distribution functions:                 ',RANGE,
     . ' Ang'

C Read number of divisions for the RDF plot

      NRBIN_DEFAULT = 200
      NBIN = FDF_INTEGER('AMD.RDF.NRBINS',NRBIN_DEFAULT)
      write(6,'(a,i5)')
     . ' Number of radial bins in distribution functions  ',NBIN

C Read FWHM of the gaussian for the RDF histogram smearing

      SIGMA_DEFAULT = 0.1D0
      SIGMA = FDF_PHYSICAL('AMD.RDF.SIGMA',SIGMA_DEFAULT,'Ang')
      write(6,'(/,a,f5.2,a)') 
     . ' Gaussian broadening:                             ',SIGMA,
     . ' Ang'

C Smoothing RDF and ANN with interpolation formula?

      SMOOTHINT = FDF_BOOLEAN('AMD.RDF.PolynomialSmoothing', .false. )
      write(6,'(/,a,l1)') 
     . ' Polynomial Smoothing of RDF and ANN?:            ', SMOOTHINT

C Read initial time step for RDF analisys

      TIMEINI = FDF_INTEGER('AMD.InitialTimeStep',0 )
      write(6,'(/,a,i8)')
     . ' Initial time step for pair distr. analysis:       ', TIMEINI
      if (TIMEINI .EQ. 0)
     . write(6,'(a)') ' The first MD time step will be used'

C Read final time step for time correlation analisys

      TIMEFIN = FDF_INTEGER('AMD.FinalTimeStep',0 )
      write(6,'(/,a,i8)')
     . ' Final time step for time distr. analysis:         ', TIMEFIN
      if (TIMEINI .EQ. 0) write(6,'(a)')
     .  ' The last MD time step will be used'

      IF (TIMEINI .GT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'AMD.InitialTimeStep must be smaller than AMD.FinalTimeStep!'
        STOP
      ENDIF


C Read interval for time origins

      IOR = FDF_INTEGER('AMD.TimeSkipInterval',1 )
      write(6,'(/,a,i8)')
     . ' Interval for time steps used in RDF analysis:   ',IOR


      RETURN

      END SUBROUTINE REDATARDF
