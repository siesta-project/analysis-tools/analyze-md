!
      subroutine readcel( cell )

c *********************************************************************
c Reads the unit cell vectors from the data file
c ********* Output ****************************************************
c real*8 cell(3,3) : Unit cell vectors ucell(ixyz,ivec)
c *********************************************************************
C
C  Modules
C
      use precision, only : dp
      use fdf

      implicit          none

      integer           mscell(3,3)
      real(dp)          alat, cell(3,3)

C Internal variables
      integer           i, iunit, ix, j
      real(dp)          alp, alplp, betlp, blp, clp,
     .                  gamlp, pi, xxx

C Lattice constant
      alat = fdf_physical('LatticeConstant',0.d0,'Ang')

      if (alat .eq. 0.0d0) then
         call die (
     $    'If VARCEL = .false. you must specify the lattice constant')
      endif

C Lattice vectors

C      if ( (.not. fdf_block('LatticeParameters',iunit)) .and.
C     .     (.not. fdf_block('LatticeVectors',iunit)) ) then
C         write(6,'(2a)')
C     .     'If VARCEL = .false. you must specify the lattice vectors'
C         call die('')
C      endif

      if ( fdf_block('LatticeParameters',iunit) .and.
     .     fdf_block('LatticeVectors',iunit) ) then
         write(6,'(2a)')'redcel: ERROR: Lattice vectors doubly ',
     .     'specified: by LatticeVectors and by LatticeParameters.' 
         call die('redcel: ERROR: Double input for lattice vectors')
      endif

      if ( fdf_block('LatticeParameters',iunit) ) then
         read(iunit,*) alp, blp, clp, alplp, betlp, gamlp
*        write(6,'(a)')
*    .    'redcel: Lattice Parameters (units of Lattice Constant) ='
*        write(6,'(a,3f10.5,3f9.3)')
*    .    'redcel: ',alp,blp,clp,alplp,betlp,gamlp
         pi = acos(-1.d0)
         alplp = alplp * pi/180.d0
         betlp = betlp * pi/180.d0
         gamlp = gamlp * pi/180.d0
         cell(1,1) = alp
         cell(2,1) = 0.d0
         cell(3,1) = 0.d0
         cell(1,2) = blp * cos(gamlp)
         cell(2,2) = blp * sin(gamlp)
         cell(3,2) = 0.d0
         cell(1,3) = clp * cos(betlp)
         xxx = (cos(alplp) - cos(betlp)*cos(gamlp))/sin(gamlp)
         cell(2,3) = clp * xxx
         cell(3,3) = clp * sqrt(sin(betlp)*sin(betlp) - xxx*xxx)
      elseif ( fdf_block('LatticeVectors',iunit) ) then
        do i = 1,3
          read(iunit,*) (cell(j,i), j=1,3)
        enddo
      else
        do i = 1,3
          do j  = 1,3
            cell(i,j) = 0.d0
          enddo
          cell(i,i) = 1.d0
        enddo
      endif
C ...

C Multiply cell vectors by by lattice constant ........................
      do i = 1,3
        do ix = 1,3
          cell(ix,i) = alat * cell(ix,i)
        enddo
      enddo
C ..................


      end

