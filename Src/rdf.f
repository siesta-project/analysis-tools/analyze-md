      SUBROUTINE RDF(NA,NS)

      IMPLICIT NONE

      integer, parameter :: dp = selected_real_kind(14,100)

      INTEGER                :: NA, NS


C Define dimensions
      INTEGER, PARAMETER     :: NX = 3
      INTEGER, PARAMETER     :: MAXNNA = 500    !Max. num. of interacting neig.

C Argument types and dimensions
      INTEGER                :: JAN(MAXNNA)
      INTEGER, DIMENSION(:), ALLOCATABLE    ::  
     .    IS,NAT
      INTEGER, DIMENSION(:,:,:), ALLOCATABLE    ::  
     .    HIST

      REAL(DP)               :: CELL(NX,NX), RANGE
      REAL(DP)               :: VCELL(NX,NX)
      REAL(DP)               :: R2IJ(MAXNNA), XIJ(NX,MAXNNA)

      REAL(DP), DIMENSION(:,:), ALLOCATABLE     ::
     .    XA,VA

      LOGICAL                :: VARCEL, SMOOTHINT

      CHARACTER(LEN=60)   :: SNAME
      CHARACTER(LEN=10), DIMENSION(:), ALLOCATABLE    :: 
     .    SYM
      CHARACTER(LEN=25)    :: FNAME

C Internal variables

      INTEGER                :: NNA, IX, JX, IA, JA, I, IN, NSTEP
      INTEGER                :: BIN, ISP, JSP, NBIN, JBIN, NBINEX, ISTEP
      INTEGER                :: UNIT1, UNIT2
      INTEGER                :: TIMEINI, TIMEFIN, IOR, NSTEPANAL

      REAL(DP)               :: RIJ, DELR, RLOWER, RUPPER, NIDEAL, H
      REAL(DP)               :: R,  RP, PI
      REAL(DP)               :: CONST, VOLCEL, SIGMA, VOLAVE, VOL
      REAL(DP), DIMENSION(:,:,:), ALLOCATABLE  :: 
     .     GR, ANN, BROAD

      CHARACTER(LEN=25)      :: PASTE

      EXTERNAL VOLCEL, PASTE, IO_ASSIGN, IO_CLOSE



C
C
      WRITE(6,'(3/,a)') 
     . ' ------------------------------------------------------------'
      WRITE(6,'(/,a)') 
     . '             COMPUTING RADIAL DISTRIBUTION FUNCTIONS         '
      WRITE(6,'(/,a)') 
     . ' ------------------------------------------------------------'



      PI = 4.0 * ATAN(1.0)


      ALLOCATE(IS(NA))
      ALLOCATE(SYM(NS))

      CALL REDATARDF( NA, NS, SNAME, VARCEL,
     .             IS, SYM, RANGE, NBIN, SIGMA, SMOOTHINT, CELL,
     .             TIMEINI, TIMEFIN, IOR)

      IF (TIMEINI .GT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'TIMEINI must be smaller than TIMEFIN!'
        STOP
      ENDIF

      DELR = RANGE/NBIN
      NBINEX = NBIN + 5*SIGMA/DELR

      ALLOCATE(NAT(NS))
      ALLOCATE(HIST(NS,NS,NBINEX))
      ALLOCATE(XA(NX,NA))
      ALLOCATE(VA(NX,NA))
      ALLOCATE(GR(NS,NS,NBINEX))
      ALLOCATE(ANN(NS,NS,NBINEX))
      ALLOCATE(BROAD(NS,NS,NBIN))


      VOLAVE = 0.0D0

      DO ISP = 1, NS
        DO JSP = 1,NS
          DO JBIN = 1,NBINEX
            HIST(ISP,JSP,JBIN)=0
            GR(ISP,JSP,JBIN)=0.0D0
            ANN(ISP,JSP,JBIN)=0.0D0
          ENDDO
        ENDDO
      ENDDO

      DO ISP=1,NS
        NAT(ISP) = 0
      ENDDO

      DO IA = 1,NA
        NAT(IS(IA))=NAT(IS(IA))+1
      ENDDO



      CALL READMD(SNAME,NA,VARCEL,0,NSTEP,XA,VA,CELL,VCELL)

      WRITE(6,'(/,a,i6,a)') 
     .   ' Your MD file contains ',NSTEP,' time steps'
      IF (NSTEP .LT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'AMD.FinalTimeStep is larger than number of MD steps on file!'
        STOP
      ENDIF

C Skip time steps in file before TIMEINI
      DO ISTEP = 1,TIMEINI-1
        CALL READMD(SNAME,NA,VARCEL,ISTEP,NSTEP,XA,VA,CELL,VCELL)
      ENDDO
        

C Skip data between time origins
      NSTEPANAL = 0
      DO ISTEP = TIMEINI,TIMEFIN
        CALL READMD(SNAME,NA,VARCEL,ISTEP,NSTEP,XA,VA,CELL,VCELL)

C Skip data between time origins
        IF ((IOR*((ISTEP-TIMEINI)/IOR)).NE.(ISTEP-TIMEINI)) 
     .      GOTO 100
        NSTEPANAL = NSTEPANAL + 1

        VOL = VOLCEL(CELL)
        VOLAVE = VOLAVE + VOL

        NNA = MAXNNA
        CALL NEIGHB( CELL, RANGE+5.0*SIGMA, 
     .               NA, XA, 0, 1, NNA, JAN, XIJ, R2IJ )
        IF (NNA .GT. MAXNNA) STOP 'MAXNNA too small'

        DO IA = 1,NA
          NNA = MAXNNA
          CALL NEIGHB( CELL, RANGE+5.0*SIGMA, 
     .                 NA, XA, IA, 0, NNA, JAN, XIJ, R2IJ )
          IF (NNA .GT. MAXNNA) STOP 'MAXNNA too small'
          DO IN = 1,NNA
            JA = JAN(IN)
            IF (JA .NE. IA) THEN
              RIJ = SQRT(R2IJ(IN))
              BIN = INT (RIJ/DELR) + 1
              HIST(IS(IA),IS(JA),BIN) = HIST(IS(IA),IS(JA),BIN) + 1
            ENDIF
          ENDDO
        ENDDO

100     CONTINUE

      ENDDO


C Calculate Average Number of Neighbours (ANN) between distance r and r+dr
C and RDF (normalized to ideal gas density)


      VOLAVE = VOLAVE/NSTEPANAL
      CONST = 4.0 * PI / VOLAVE / 3.0D0
      DO BIN = 1, NBINEX
        RLOWER = REAL (BIN-1) * DELR
        RUPPER = RLOWER + DELR
        NIDEAL = CONST * (RUPPER ** 3 - RLOWER ** 3)
        DO ISP = 1,NS
          DO JSP = 1,NS
            H = HIST(ISP,JSP,BIN)
            ANN(ISP,JSP,BIN) = REAL(H) / REAL(NAT(ISP)) / NSTEPANAL
            GR(ISP,JSP,BIN) = ANN(ISP,JSP,BIN) / 
     .                        REAL(NAT(JSP)) / NIDEAL
            ANN(ISP,JSP,BIN) = ANN(ISP,JSP,BIN) / DELR
          ENDDO
        ENDDO
      ENDDO


C GAUSSIAN BROADENING OF RDF AND ANN


      IF (DABS(SIGMA) .GT. 1.0D-8) THEN

        CALL GAUSS(NS,NBIN,NBINEX,SIGMA,DELR,GR,BROAD)
        CALL GAUSS(NS,NBIN,NBINEX,SIGMA,DELR,ANN,BROAD)

      ENDIF



      IF (SMOOTHINT) THEN

        CALL SMOOTH(NS,NBIN,NBINEX,GR,BROAD)
        CALL SMOOTH(NS,NBIN,NBINEX,ANN,BROAD)

      ENDIF


      write(6,'(/,a)') 
     . ' Radial Distribution Function for species X and Y'
      write(6,'(a)') ' stored in files X-Y.RDF'
      write(6,'(/,a)') 
     . ' Average Number of Neighbours for species X and Y'
      write(6,'(a)') ' stored in files X-Y.ANN'

      DO ISP = 1,NS
        DO JSP = 1,NS
          FNAME=PASTE(SYM(ISP),'-')
          FNAME=PASTE(FNAME,SYM(JSP))
          FNAME=PASTE(FNAME,'.RDF')
          CALL IO_ASSIGN(UNIT1)
          CALL IO_ASSIGN(UNIT2)
          OPEN(UNIT1, FILE=FNAME, STATUS='UNKNOWN')
          FNAME=PASTE(SYM(ISP),'-')
          FNAME=PASTE(FNAME,SYM(JSP))
          FNAME=PASTE(FNAME,'.ANN')
          OPEN(UNIT2, FILE=FNAME, STATUS='UNKNOWN')
          REWIND(UNIT1)
          REWIND(UNIT2)
          DO BIN = 1,NBIN
            RLOWER = REAL (BIN-1) * DELR
            WRITE(UNIT1,*) RLOWER + DELR/2 , GR(ISP,JSP,BIN)
            WRITE(UNIT2,*) RLOWER + DELR/2 , ANN(ISP,JSP,BIN)
          ENDDO
          CALL IO_CLOSE(UNIT1)
          CALL IO_CLOSE(UNIT2)
        ENDDO
      ENDDO



      DEALLOCATE(BROAD)
      DEALLOCATE(ANN)
      DEALLOCATE(GR)
      DEALLOCATE(VA)
      DEALLOCATE(XA)
      DEALLOCATE(HIST)
      DEALLOCATE(NAT)
      DEALLOCATE(SYM)
      DEALLOCATE(IS)


      RETURN
      END




      SUBROUTINE SMOOTH(NS,NBIN,NBINEX,GR,BROAD)
C Smoothing with third degree polynomial, five point smoothing
C (Allen-Tildesley 1990, pg 203-204).


      IMPLICIT NONE
      integer, parameter :: dp = selected_real_kind(14,100)
      INTEGER NS,NBIN,NBINEX
      INTEGER ISP,JSP,BIN
      REAL(DP)  GR(NS,NS,NBINEX),BROAD(NS,NS,NBIN)

      DO ISP = 1,NS
        DO JSP = 1,NS

          BROAD(ISP,JSP,1) = (1.0D0/70.0D0)*
     .     (   69.0D0*GR(ISP,JSP,1) +  4.0D0*GR(ISP,JSP,2)
     .       -  6.0D0*GR(ISP,JSP,3) +  4.0D0*GR(ISP,JSP,4)
     .       -  1.0D0*GR(ISP,JSP,5) )

          BROAD(ISP,JSP,2) = (1.0D0/35.0D0)*
     .     (    2.0D0*GR(ISP,JSP,1) + 27.0D0*GR(ISP,JSP,2)
     .       + 12.0D0*GR(ISP,JSP,3) -  8.0D0*GR(ISP,JSP,4)
     .       +  2.0D0*GR(ISP,JSP,5) )

          DO BIN = 3,NBIN-2
            BROAD(ISP,JSP,BIN) = (1.0D0/35.0D0)*
     .       ( -  3.0D0*GR(ISP,JSP,BIN-2) + 12.0D0*GR(ISP,JSP,BIN-1)
     .         + 17.0D0*GR(ISP,JSP,BIN)   + 12.0D0*GR(ISP,JSP,BIN+1)
     .         -  3.0D0*GR(ISP,JSP,BIN+2) )
          ENDDO

          BROAD(ISP,JSP,NBIN-1) = (1.0D0/35.0D0)*
     .     (    2.0D0*GR(ISP,JSP,NBIN)   + 27.0D0*GR(ISP,JSP,NBIN-1)
     .       + 12.0D0*GR(ISP,JSP,NBIN-2) -  8.0D0*GR(ISP,JSP,NBIN-3)
     .       +  2.0D0*GR(ISP,JSP,NBIN-4) )

          BROAD(ISP,JSP,NBIN) = (1.0D0/70.0D0)*
     .     (   69.0D0*GR(ISP,JSP,NBIN)   +  4.0D0*GR(ISP,JSP,NBIN-1)
     .       -  6.0D0*GR(ISP,JSP,NBIN-2) +  4.0D0*GR(ISP,JSP,NBIN-3)
     .       -  1.0D0*GR(ISP,JSP,NBIN-4) )

        ENDDO
      ENDDO

      DO ISP = 1,NS
        DO JSP = 1,NS
          DO BIN = 1,NBIN
            GR(ISP,JSP,BIN) = BROAD(ISP,JSP,BIN)
          ENDDO
        ENDDO
      ENDDO

      RETURN
      END


      SUBROUTINE GAUSS(NS,NBIN,NBINEX,SIGMA,DELR,GR,
     .                 BROAD)

      IMPLICIT NONE
      integer, parameter :: dp = selected_real_kind(14,100)
      INTEGER NS,NBIN
      INTEGER ISP,JSP,BIN, JBIN, NBINEX
      REAL(DP)  GR(NS,NS,NBINEX),BROAD(NS,NS,NBIN)
      REAL(DP)  SIGMA,CONST,R,RP,D,GAUSSIAN,PI,DELR,TOL

      PI = 4.0 * ATAN(1.0)
      TOL = 15.0D0

      CONST = SIGMA * DSQRT(2.0D0 * PI)

      DO ISP = 1,NS
        DO JSP = 1,NS
          DO BIN = 1,NBIN
            R = REAL (BIN-1) * DELR + DELR/2
            BROAD(ISP,JSP,BIN)=0.0D0
            DO JBIN = 1,NBINEX
              RP = REAL (JBIN-1) * DELR + DELR/2
              D = (((R-RP)/SIGMA)**2.0D0)/2.0D0
              IF (D .GT. TOL) THEN
                GAUSSIAN = 0.0D0
              ELSE
                GAUSSIAN = DELR * DEXP(-D) / CONST
              ENDIF
              BROAD(ISP,JSP,BIN)=BROAD(ISP,JSP,BIN)+
     .               GAUSSIAN*GR(ISP,JSP,JBIN)
            ENDDO
          ENDDO
        ENDDO
      ENDDO

      DO ISP = 1,NS
        DO JSP = 1,NS
          DO BIN = 1,NBIN
            GR(ISP,JSP,BIN) = BROAD(ISP,JSP,BIN)
          ENDDO
        ENDDO
      ENDDO

      RETURN
      END
