
      USE DIFFUSION

      INTEGER NA,NS,IOPT


      WRITE(6,'(2/,a)')
     . '          ***************************************         '
      WRITE(6,'(a)')
     . '          *                                     *         '
      WRITE(6,'(a)')
     . '          *       WELCOME TO ANALIZE-MD         *         '
      WRITE(6,'(a)')
     . '          *                                     *         '
      WRITE(6,'(a)')
     . '          *     Version 0.1.3   12/09/08        *         '
      WRITE(6,'(a)')
     . '          *                                     *         '
      WRITE(6,'(a)')
     . '          ***************************************         '
      WRITE(6,'(a)') ''




      WRITE(6,'(2/,a)') ' Reading basic system data'
      CALL REDATA(NA,NS)

      CALL RDF(NA,NS)

      IOPT = 1
      CALL CORRFUNC(NA,NS,IOPT)

      IOPT = 2
      CALL CORRFUNC(NA,NS,IOPT)

      WRITE(6,'(2/,a)')
     . '          ***************************************         '
      WRITE(6,'(a)')
     . '          *       CALCULATION COMPLETED         *         '
      WRITE(6,'(a)')
     . '          ***************************************         '

      STOP

      END
