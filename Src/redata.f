      SUBROUTINE REDATA( NA, NS)

C **********************************************************************
C Read the data file to obtain number of atoms and number of species
C These will be needed to allocate matrices
C P. Ordejon, February 2008
C **********************************************************************

      USE FDF
      USE CHEMICAL

      IMPLICIT NONE

      INTEGER, INTENT(OUT) :: 
     .   NA, NS

      CHARACTER
     .  FILEIN*50, FILEOUT*50



C **** INPUT ***********************************************************
C **** OUTPUT **********************************************************
C **********************************************************************

      FILEIN = 'stdin'
      FILEOUT = 'md-analyzer.fdflog'
      CALL FDF_INIT(FILEIN,FILEOUT)

C Number of atoms
      NA = FDF_INTEGER('NumberOfAtoms',0)
      IF (NA.EQ.0) CALL DIE("Must specify number of atoms!")
      write(6,'(/,a,i8)') ' Number of atoms:   ',NA

C Number of species
      NS = FDF_INTEGER('Number_of_species',0)
      IF (NS.EQ.0) CALL DIE("No species found!!!")
      write(6,'(a,i8)')   ' Number of species: ',NS

      END SUBROUTINE REDATA
