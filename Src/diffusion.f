
      MODULE DIFFUSION

!
! ================================================================
! DESCRIPTION OF THE MODULE
! ================================================================
!
! This module calculates several time correlation functions from 
! MD files obtained from SIESTA:
!
! - Velocity-velocity autocorrelation function  (VAC)
!
!   VAC(t) = < v(t).v(0) >
!
! - Power spectrum, related to the vibrational densirt of states
!   (VDOS) as the Fourier transform of the VAC:
!
!   VDOS(w) = 1/pi Int [cos(wt) * VAC(t)/VAC(0)] dt
!
! - Mean Square Displacement (MSD)
!
!   MSD(t) = < [r(t)-r(0)]**2 >
!
! and the transport coefficients related to those:
! 
! - Diffusion Coefficient
!
!   D = 1/3 Int dt VAC/t)
!   2Dt = 1/3 MSD(t)
!
! P. Ordejon, Feb. 2008
! --------------------------------------------------------------------
!
! How to use it:
!
! - The module is accessed from the calling program via an explicit call:
!
!   CALL CORRFUNC(NA,NS,IOPT)
!
!   NA = Number of atoms in the simulation
!   NS = Number of chemical species in the simulation
!   IOPT = 1 (to compute VAC), 2 (to compute MSD)
! 
! - The module will read the relevant data from two files:
! 
!   - SystemLabel.fdf (info related to the system and the 
!                      required analysis)
!   - SystemLabel.MD  (the Siesta file containinf the MD info).
!
! - The module will write files named X.* (X being the symbol of 
!   each atomic species) with the output of the different 
!   quantities computed for the corresponding atomic species, 
!   and will dump on standard output some information related 
!   to the calculation.
!
! ---------------------------------------------------------------------
!
! ================================================================
! DESCRIPTION OF PROCEDURES IN MODULE
! ================================================================
!
! CORRFUNC    : INTERFACE TO THE MODULE FROM OUTSIDE
!               -CALLS ROUTINE REDATACOR TO OBTAIN THE RELEVANT
!                INFO FROM THE FDF FILE
!               -CALLS READMD (EXTERNAL TO THE MODULE) TO ANALIZE
!                THE INFO IN THE MD FILE, AND POSITION IT TO
!                THE RELEVANT TIME STEP
!               -CALLS TCORR FOR EACH CHEMICAL SPECIES TO COMPUTE
!                THE CORRESPONDING TIME CORRELATION FUNCTION
!               -WRITES THE RESULTS IN X.* FILES
!
! TCORR       : LOOPS OVER TIME STEPS TO CALCULATE THE CORRELATION
!               FUNCTIONS. AT EACH TIME STEP:
!               -CALLS GETDATA TO OBTAIN THE FUNCTION (COORDINATES
!                OR VELOCITIES FROM MD FILE)
!               -CALLS STORE TO STORE DATA READ FOR FUTURE ANALISYS
!               -CALLS CORR TO COMPUTE THE APPROPRIATE CORRELATION
!                FUNCTION FOR THE PRESENT TIME STEP
!
! STORE       : STORES FUNCTION (COORS OR VELOS) FOR FUTURE ANALISYS
!
! CORR        : CALCULATES THE APPROPRIATE CORRELATION FOR PRESENT
!               TIME STEP
!
! GETDATA     : READS THE POSITIONS AND VELOCITIES FOR CURRENT TIME
!               STEPS AND RETURS THE APPROPRIATE VALUE (X OR V) 
!
! REDATACOR   : READS THE INPUT PARAMETERS FROM SystemLabel.MD FILE 
!
! POWER       : FOURIER-TRANSFORMS THE V-V AUTOCORRELATION AND
!               NORMALIZES IT TO OBTAIN THE VIBRATIONAL DOS
!
! ================================================================
! DESCRIPTION OF MODULE VARIABLES
! ================================================================
! 
! FIXED SIZE VARIABLES
!
! PARAMETERS:-----------------------------------------------------
!
! DP            = DOUBLE PRECISSION
! NX            = 3 (THREE DIMENSIONS OF SPACE)
! ----------------------------------------------------------------
!
! INTEGER SAVE:---------------------------------------------------
!
! INDEX          = INDEX OF SPECIES BEING ANALIZED
! INDSTEP        = CURRENT STEP INDEX FROM DATA (MD) FILE
! NATOMS         = NA, NUMBER OF ATOMS
! NSTEPTAPE      = NUMBER OF TIME STEPS STORED IN MD FILE
! NSTEPANAL      = NUMBER OF TIME STEPS USED IN THE ANALYSIS
! ----------------------------------------------------------------
!
! CHARACTER:------------------------------------------------------
!
! SNAME(LEN=60)  = NAME OF INPUT FDF FILE (SystemLabel.fdf)
! ----------------------------------------------------------------
!
! LOGICAL: -------------------------------------------------------
!
! VARCEL         = VARIABLE CELL RUN? (.T. = YES, .F.= NO)
! ----------------------------------------------------------------
!
! REAL(DP): ------------------------------------------------------
!
! CELL(NX,NX)    = CELL VECTORS
! VCELL(NX,NX)   = CELL VELOCITIES
! ----------------------------------------------------------------
!
!
! ALLOCATABLE SIZE VARIABLES 
!
! INTEGER --------------------------------------------------------
!
! IS(NA)         = INDEX OF SPECIES OF EACH ATOM 
! NAT(NS)        = NUMBER OF ATOMS OF EACH SPECIES
! ----------------------------------------------------------------
!
! CHARACTER(LEN=10) ----------------------------------------------
!
! SYM(NS)(LEN=10)= ATOMIC SYMBOL OF EACH SPECIES
! ----------------------------------------------------------------
!
! REAL(DP)  ------------------------------------------------------
! 
! AMASS(NS)      = ATOMIC MASS OF EACH SPECIES
! XA(NX,NA)      = ATOMIC COORDINATES OF EACH ATOM
! VA(NX,NA)      = ATOMIC VELOCITIES OF EACH ATOM
! STORX(:,NA)    = STORAGE SPACE FOR CORRELATION FUNCTIONS
! STORY(:,NA)    = STORAGE SPACE FOR CORRELATION FUNCTIONS
! STORZ(:,NA)    = STORAGE SPACE FOR CORRELATION FUNCTIONS
! CF(NT)         = CORRELATION FUNCTION (NT = NUMBER OF TIME VALUES)
! X(NA)          = STORAGE OF INSTANTANEOUS X ATOMIC POS/VEL
! Y(NA)          = STORAGE OF INSTANTANEOUS Y ATOMIC POS/VEL
! Z(NA)          = STORAGE OF INSTANTANEOUS Z ATOMIC POS/VEL
! ----------------------------------------------------------------
!
!
! POINTERS
!
! REAL(DP) -------------------------------------------------------
!
! DATA(:,:)      = POINTS TO XA OR VA
!
! ----------------------------------------------------------------
!


      IMPLICIT NONE

      PUBLIC  CORRFUNC
      PRIVATE TCORR, STORE, CORR, GETDATA, REDATACOR

C  Variables

C FIXED SIZE VARIABLES

      INTEGER, PARAMETER     :: DP = SELECTED_REAL_KIND(14,100)

      INTEGER, PARAMETER     :: NX = 3

      INTEGER,SAVE           :: INDSTEP, NSTEPTAPE, NSTEPANAL, ISPEC,
     .                          NATOMS

      CHARACTER(LEN=60),SAVE :: SNAME

      LOGICAL                :: VARCEL

      REAL(DP)               :: CELL(NX,NX), VCELL(NX,NX)


C ALLOCATABLE VARIABLES

      INTEGER,  DIMENSION(:), SAVE,  ALLOCATABLE      ::   IS,NAT

      CHARACTER(LEN=10), DIMENSION(:),  ALLOCATABLE, SAVE  ::   SYM

      REAL(DP), DIMENSION(:),   SAVE,   ALLOCATABLE   ::   AMASS
      REAL(DP), DIMENSION(:,:), SAVE, TARGET, ALLOCATABLE  ::   XA, VA


      REAL(DP), DIMENSION(:,:), SAVE, ALLOCATABLE ::   STORX,STORY,STORZ
      REAL(DP), DIMENSION(:),   SAVE, ALLOCATABLE     ::   CF
      REAL(DP), DIMENSION(:),   SAVE, ALLOCATABLE     ::   X,Y,Z
      REAL(DP), DIMENSION(:),   SAVE, ALLOCATABLE     ::   ANORM


C POINTERS

      REAL(DP), DIMENSION(:,:), POINTER              ::   DATA







      CONTAINS





      SUBROUTINE CORRFUNC(NA,NS,IOPT)


      INTEGER                :: NS, IOPT
      INTEGER, TARGET        :: NA


C  INTERNAL VARIABLES

      INTEGER                ::  IA, IFUNC, IOR, ISTEP, NT, NW,
     .                           TIMEINI, TIMEFIN, UNIT
      INTEGER, TARGET        ::  ISP

      CHARACTER(LEN=25)      ::  FNAME
      CHARACTER(LEN=100)     ::  PASTE, PASTEB, MESSG1, MESSG2
      CHARACTER(LEN=4)       ::  SUFFIX

      REAL(DP)               ::  DT, TIME, WMIN, WMAX

      REAL(DP), DIMENSION(:), ALLOCATABLE     ::  INTCF

      EXTERNAL PASTE, PASTEB


      WRITE(6,'(3/,a)')
     . ' ------------------------------------------------------------'
      IF (IOPT .EQ. 1) THEN
      WRITE(6,'(/,a)')
     . '         COMPUTING VELOCITY-VELOCITY AUTOCORRELATION         '
      ELSE IF (IOPT .EQ. 2) THEN
      WRITE(6,'(/,a)')
     . '         COMPUTING ATOMIC MEAN SQUARE DISPLACEMENT           '
      ELSE 
      STOP ' ERROR: Wrong IOPT'
      ENDIF
      WRITE(6,'(/,a)')
     . ' ------------------------------------------------------------'




      NATOMS = NA

      ALLOCATE(IS(NA))
      ALLOCATE(SYM(NS))
      ALLOCATE(AMASS(NS))
      ALLOCATE(NAT(NS))
      ALLOCATE(XA(NX,NA))
      ALLOCATE(VA(NX,NA))

      IF (IOPT .EQ. 1) THEN
        DATA => VA
        IFUNC = 1
      ELSE IF(IOPT .EQ. 2) THEN
        DATA => XA
        IFUNC = 2
      ELSE 
        STOP ' ERROR: WRONG IOPT IN CORRFUNC'
      ENDIF

      CALL REDATACOR( NA, NS, DT, TIMEINI, TIMEFIN, IOR, NT, NW,
     .                WMIN, WMAX)

      IF (TIMEINI .GT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'TIMEINI must be smaller than TIMEFIN!'
        STOP
      ENDIF


      DO ISP=1,NS
        NAT(ISP) = 0
      ENDDO

      DO IA = 1,NA
        NAT(IS(IA))=NAT(IS(IA))+1
      ENDDO

      INDSTEP = 0
      CALL READMD(SNAME,NA,VARCEL,INDSTEP,NSTEPTAPE,XA,VA,CELL,VCELL)

      WRITE(6,'(/,a,i6,a)')
     .   ' Your MD file contains ',NSTEPTAPE,' time steps'

      IF (NSTEPTAPE .LT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'AMD.FinalTimeStep is larger than number of MD steps on file!'
        STOP
      ENDIF

      IF (TIMEINI .EQ. 0) TIMEINI = 1
      IF (TIMEFIN .EQ. 0) TIMEFIN = NSTEPTAPE
      IF (NT .EQ. 0) NT =TIMEFIN - TIMEINI + 1
      IF (NT .GT. NSTEPTAPE) THEN
        WRITE(6,'(/,2a)') 
     .  ' ERROR: ',
     .  'AMD.TIMEFUNCS.NTbins can not be larger than'
        WRITE(6,'(a,/)')
     .  '        number of steps on MD file'
        STOP
      ENDIF
      NSTEPANAL = TIMEFIN - TIMEINI +1
      IF (NSTEPANAL .LT. NT) THEN
        WRITE(6,'(/,a)')
     .  ' ERROR: AMD.TIMEFUNCS.NTbins can not be larger than'
        WRITE(6,'(a,/)')
     .  '        AMD.FinalTimeStep - AMD.InitialTimeStep'
        STOP
      ENDIF
   

      ALLOCATE(CF(NT))
      ALLOCATE(INTCF(NT))



      DO ISP = 1,NS

        ISPEC = ISP
        INDSTEP = 0

C CALL READMD WITH INDSTEP = 0 TO REWIND MD FILE

        CALL READMD(SNAME,NA,VARCEL,INDSTEP,NSTEPTAPE,
     .              XA,VA,CELL,VCELL)
C SKIP DATA UP TO INITIAL TIME STEP FOR ANALYSIS

        DO ISTEP = 1, TIMEINI - 1
          INDSTEP = INDSTEP + 1
          CALL READMD(SNAME,NA,VARCEL,INDSTEP,NSTEPTAPE,
     .                XA,VA,CELL,VCELL)
        ENDDO

        CALL TCORR (NAT(ISP), IOR, NT, IFUNC)

        IF (IOPT .EQ. 1) SUFFIX='.VAC'
        IF (IOPT .EQ. 2) SUFFIX='.MSD'
        FNAME=PASTE(SYM(ISP),SUFFIX)
        CALL IO_ASSIGN(UNIT)
        OPEN(UNIT, FILE=FNAME, STATUS='UNKNOWN')
        REWIND(UNIT)


        IF (IOPT .EQ. 1) THEN

          CALL POWER(SYM(ISP),NT, DT, NW, WMIN, WMAX)


          MESSG1=PASTEB(' V-V Autocorr. Function for species ',SYM(ISP))
          MESSG2=PASTE(MESSG1,' stored in file ')
          MESSG1=PASTEB(MESSG2,SYM(ISP))
          MESSG2=PASTE(MESSG1,'.VAC')
          WRITE(6,'(/,a)') MESSG2
          
          WRITE(UNIT,'(a)') '# VAC: Velocity-velocity autocorrelation'
          WRITE(UNIT,'(3a20)') '#          TIME (fs)',
     .                         'VAC ((Ang/fs)**2)','DC (Ang**2/fs)'


          DO ISTEP = 1, NT

C INTEGRATE USING TRAPEZOIDAL RULE

            IF (ISTEP .EQ. 1) THEN
              INTCF(ISTEP)=DT*((3.0D0/8.0D0)*CF(ISTEP)+
     .                         (1.0D0/8.0D0)*CF(ISTEP+1))
            ELSE IF (ISTEP .EQ. NT) THEN
              INTCF(ISTEP)=INTCF(ISTEP-1)+
     .                DT*((3.0D0/8.0D0)*CF(ISTEP)+
     .                    (1.0D0/8.0D0)*CF(ISTEP-1))
            ELSE
              INTCF(ISTEP)=INTCF(ISTEP-1)+
     .                DT*((3.0D0/4.0D0)*CF(ISTEP)+
     .                    (1.0D0/8.0D0)*(CF(ISTEP-1)+CF(ISTEP+1)))
            ENDIF

            TIME = (ISTEP-1)*DT
            WRITE(UNIT,'(3f20.12)') TIME,CF(ISTEP), INTCF(ISTEP)/3.0D0
          ENDDO
        ENDIF


        IF (IOPT .EQ. 2) THEN


          MESSG1=PASTEB(' Mean Square Displacement for species ',
     .                  SYM(ISP))
          MESSG2=PASTE(MESSG1,' stored in file ')
          MESSG1=PASTEB(MESSG2,SYM(ISP))
          MESSG2=PASTE(MESSG1,'.MSD')
          WRITE(6,'(/,a)') MESSG2

          WRITE(UNIT,'(a)') '# MSD: Mean Square Displacement'
          WRITE(UNIT,'(a)') '# DC: Diffusion coefficient = (1/6) MSD/T'    
          WRITE(UNIT,'(3a20)') '#          TIME (fs)',
     .                         'MSD (Ang**2)','DC (Ang**2/fs)'
          WRITE(UNIT,'(3f20.12)') 0.0D0,CF(1),0.0D0
          DO ISTEP = 2, NT
            TIME = (ISTEP-1)*DT
            WRITE(UNIT,'(3f20.12)') TIME,CF(ISTEP),
     .                              CF(ISTEP)/(TIME*6.0D0)
          ENDDO
        ENDIF

        CALL IO_CLOSE(UNIT)
        
      ENDDO


      DEALLOCATE(INTCF)
      DEALLOCATE(CF)
      DEALLOCATE(IS)
      DEALLOCATE(SYM)
      DEALLOCATE(AMASS)
      DEALLOCATE(NAT)
      DEALLOCATE(XA)
      DEALLOCATE(VA)

      RETURN
      END SUBROUTINE CORRFUNC



        SUBROUTINE TCORR(N,IOR,NT,IFUNC)

C    *******************************************************************
C    ** CALCULATION OF TIME CORRELATION FUNCTIONS.                    **
C    **                                                               **
C    ** THIS PROGRAM ANALYZES DATA TO CALCULATE A TIME CORRELATION    **
C    ** FUNCTION IN ONE SWEEP (LOW MEMORY REQUIREMENT). IN THIS       **
C    ** EXAMPLE THE VELOCITY AUTO-CORRELATION FUNCTION IS CALCULATED. **
C    **                                                               **
C    ** PRINCIPAL VARIABLES:                                          **
C    **                                                               **
C    ** INTEGER  N                  NUMBER OF ATOMS                   **
C    ** INTEGER  NSTEP              NUMBER OF STEPS ON THE TAPE       **
C    ** INTEGER  IOR                INTERVAL FOR TIME ORIGINS         **
C    ** INTEGER  NT                 CORRELATION LENGTH, INCLUDING T=0 **
C    ** INTEGER  NTIMOR             NUMBER OF TIME ORIGINS            **
C    ** INTEGER  NLABEL             LABEL FOR STEP (1,2,3...NSTEP)    **
C    ** REAL     VX(N),VY(N),VZ(N)  VELOCITIES                        **
C    ** REAL     VACF(NT)           THE CORRELATION FUNCTION          **
C    ** NSTEP AND NT SHOULD BE MULTIPLES OF IOR.                      **
C    **                                                               **
C    ** ROUTINES REFERENCED:                                          **
C    **                                                               **
C    ** SUBROUTINE STORE ( J1 )                                       **
C    **    ROUTINE TO STORE THE DATA FOR CORRELATION                  **
C    ** SUBROUTINE CORR ( J1, J2, IT )                                **
C    **    ROUTINE TO CORRELATE THE STORED TIME ORIGINS               **
C    **                                                               **
C    ** USAGE:                                                        **
C    **                                                               **
C    ** DATA IN FILE DFILE ON FORTRAN UNIT DUNIT.                     **
C    ** RESULTS IN FILE RFILE ON FORTRAN UNIT RUNIT.                  **
C    *******************************************************************


        IMPLICIT NONE

        INTEGER N, IOR, NT, IFUNC

C  Internal variables

        INTEGER     NDIM, NTIMOR
        INTEGER     FULLUP

        INTEGER,DIMENSION(:),ALLOCATABLE   ::     S, TM
        INTEGER     TS, TSS, L, NINCOR, K, JA, IB, IN, IA, JO, I
        INTEGER     NLABEL

C    *******************************************************************

        NDIM = NT / IOR + 1
        NTIMOR = NSTEPANAL / IOR
        FULLUP = NDIM - 1 

        ALLOCATE(S(NTIMOR))
        ALLOCATE(TM(NTIMOR))

        ALLOCATE(ANORM(NT))
        ALLOCATE(STORX(NDIM,N))
        ALLOCATE(STORY(NDIM,N))
        ALLOCATE(STORZ(NDIM,N))
        ALLOCATE(X(N))
        ALLOCATE(Y(N))
        ALLOCATE(Z(N))


C    ** INITIALIZE COUNTERS **

        NINCOR = FULLUP
        JA = 1
        IA = 1
        IB = 1

C    ** ZERO ARRAYS **

        DO 5 I = 1, NT

           CF(I)  = 0.0D0
           ANORM(I) = 0.0D0

5       CONTINUE

C   ** CALCULATION BEGINS **

        DO 40 L = 1, NTIMOR

           JA   = JA + 1
           S(L) = JA - 1

           CALL GETDATA(NLABEL, X, Y, Z)

           TM(L) = NLABEL

C       ** STORE STEP AS A TIME ORIGIN **

           CALL STORE ( N, JA )

C       ** CORRELATE THE ORIGINS IN STORE **

           DO 10 IN = IA, L

              TSS = TM(L) - TM(IN)
              TS  = TSS + 1
              JO  = S(IN) + 1
              CALL CORR ( N, JO, JA, TS, IFUNC )

10         CONTINUE

C       ** READ IN DATA BETWEEN TIME ORIGINS. THIS CAN  **
C       ** BE CONVENIENTLY STORED IN ELEMENT 1 OF THE   **
C       ** ARRAYS STORX ETC. AND CAN THEN BE CORRELATED **
C       ** WITH THE TIME ORIGINS.                       **

           DO 30 K = 1, IOR - 1

              CALL GETDATA(NLABEL, X, Y, Z)

              CALL STORE ( N, 1 )

              DO 20 IN = IA, L

                 TSS = NLABEL - TM(IN)
                 TS  = TSS + 1
                 JO  = S(IN) + 1
                 CALL CORR ( N, JO, 1, TS, IFUNC )

20            CONTINUE

30         CONTINUE

           IF ( L .GE. FULLUP ) THEN

              IF ( L .EQ. NINCOR ) THEN

                 NINCOR = NINCOR + FULLUP
                 JA     = 1

              ENDIF

              IA = IA + 1

           ENDIF

40      CONTINUE


C    ** NORMALISE CORRELATION FUNCTIONS **

        CF(1) = CF(1) / ANORM(1) / REAL ( N )

        DO 50 I = 2, NT

C           CF(I) = CF(I) / ANORM(I) / REAL ( N ) / CF(1)
           CF(I) = CF(I) / ANORM(I) / REAL ( N ) 

50      CONTINUE

C        CF(1) = 1.0D0

        DEALLOCATE(Z)
        DEALLOCATE(Y)
        DEALLOCATE(X)
        DEALLOCATE(STORZ)
        DEALLOCATE(STORY)
        DEALLOCATE(STORX)
        DEALLOCATE(ANORM)
        DEALLOCATE(S)
        DEALLOCATE(TM)

        RETURN
        END SUBROUTINE



        SUBROUTINE STORE ( N, J1 )

C    *******************************************************************
C    ** SUBROUTINE TO STORE TIME ORIGINS                              **
C    *******************************************************************

        INTEGER     J1, N
C
        INTEGER     I


        DO 10 I = 1, N

           STORX(J1,I) = X(I)
           STORY(J1,I) = Y(I)
           STORZ(J1,I) = Z(I)

10      CONTINUE

        RETURN
        END SUBROUTINE



        SUBROUTINE CORR ( N, J1, J2, IT, IFUNC )

C    *******************************************************************
C    ** SUBROUTINE TO CORRELATE TIME ORIGINS                          **
C    *******************************************************************

        INTEGER     J1, J2, IT, IFUNC, N

        INTEGER     I

C    *******************************************************************


        DO 10 I = 1, N

           IF (IFUNC .EQ. 1) THEN 
             CF(IT) = CF(IT) + STORX(J1,I) * STORX(J2,I)
     :                       + STORY(J1,I) * STORY(J2,I)
     :                       + STORZ(J1,I) * STORZ(J2,I)
           ELSE IF (IFUNC .EQ. 2) THEN
             CF(IT) = CF(IT) + (STORX(J1,I) - STORX(J2,I))**2.0D0
     :                       + (STORY(J1,I) - STORY(J2,I))**2.0D0
     :                       + (STORZ(J1,I) - STORZ(J2,I))**2.0D0
           ELSE 
             STOP 'WRONG OPTION FOR CORRELATION FUNCTION'
           ENDIF

10      CONTINUE

        ANORM(IT) = ANORM(IT) + 1.0

        RETURN
        END SUBROUTINE




      SUBROUTINE GETDATA(NLABEL, X, Y, Z)


      INTEGER                 :: NLABEL
      REAL(DP), DIMENSION(:)  :: X,Y,Z

C INTERNAL VARIABLES
      INTEGER                 :: IA, IAT, ISTEP, IX
      REAL(DP)                :: MT, XCM(3), VCM(3)

      INDSTEP = INDSTEP + 1
      CALL READMD
     .       (SNAME,NATOMS,VARCEL,INDSTEP,NSTEPTAPE,XA,VA,CELL,VCELL)


C CORRECT POSITIONS AND VELOCITIES TO CANCEL ANY DRIFT OF THE
C CENTER OF MASS OF THE SYSTEM. 

      MT = 0.0D0
      DO IA = 1,NATOMS
        MT = MT + AMASS(IS(IA))
      ENDDO

      DO IX = 1,3
        DO IA = 1,NATOMS
          XCM(IX) = XCM(IX) + AMASS(IS(IA))*XA(IX,IA)
          VCM(IX) = VCM(IX) + AMASS(IS(IA))*VA(IX,IA)
        ENDDO
        XCM(IX) = XCM(IX)/MT
        VCM(IX) = VCM(IX)/MT
      ENDDO

      WRITE(66,'(i6,3f13.8)') INDSTEP,(XCM(IX),IX=1,3)
      WRITE(67,'(i6,3f13.8)') INDSTEP,(VCM(IX),IX=1,3)

      DO IX = 1,3
        DO IA = 1,NATOMS
          XA(IX,IA) = XA(IX,IA) - XCM(IX)
          VA(IX,IA) = VA(IX,IA) - VCM(IX)
        ENDDO
      ENDDO
      

      NLABEL = INDSTEP

      IAT = 0
      DO IA = 1, NATOMS
        IF (IS(IA) .EQ. ISPEC) THEN
          IAT = IAT +1

C  DATA IS A POINTER POINTING AT XA OR VA (AS DEFINED IN CORRFUNC)
          X(IAT) = DATA(1,IA)
          Y(IAT) = DATA(2,IA)
          Z(IAT) = DATA(3,IA)

        ENDIF
      ENDDO

      RETURN
      END SUBROUTINE




      SUBROUTINE REDATACOR( NA, NS, DT, TIMEINI, TIMEFIN, IOR, NT, NW,
     .                      WMIN, WMAX)

C **********************************************************************
C Read the data file to calculate the Velocity-velocity autocorrelation
C from the SIESTA MD file.
C P. Ordejon, February 2008
C **********************************************************************

      USE FDF
      USE CHEMICAL
      USE PERIODIC_TABLE

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: 
     .   NA, NS

      INTEGER, INTENT(OUT) :: 
     .   TIMEINI, TIMEFIN, IOR, NT, NW

      DOUBLE PRECISION, INTENT(OUT) ::
     .  DT, WMIN, WMAX

      CHARACTER(LEN=100) PASTE

      EXTERNAL PASTE



C **** INPUT ***********************************************************
C **** OUTPUT **********************************************************
C **********************************************************************

C Internal Variables
      INTEGER I, IA, IUNIT, NSP, Z
      DOUBLE PRECISION XA(3)



C Assign the SystemLabel (name of .MD file)
      SNAME = FDF_STRING('SystemLabel','siesta')


      WRITE(6,'(2/,a)')
     . ' Reading system data needed for calculation of time correl.'
      WRITE(6,'(2a)')
     . ' from input fdf file and from ',
     .  PASTE(SNAME,'.MD dynamics file')





C Variable cell run?
      VARCEL = FDF_BOOLEAN('AMD.VariableCell', .false. )
      write(6,'(/,a,l1)') 
     . ' Variable Cell run?:                              ',VARCEL
      if (VARCEL) then
       write(6,'(a)') ' Cell vectors will be read from MD file, and'
       write(6,'(a,/)') ' values from input fdf file will NOT be used!'
      else
       write(6,'(a,/)') ' Cell vectors will be read from input fdf file'
      endif

C Read AtomicCoordinatesAndAtomicSpecies block

      IF ( FDF_BLOCK('AtomicCoordinatesAndAtomicSpecies',IUNIT) )
     .     THEN
C XA: Dummy vector; values not used.
         DO IA = 1, NA 
            READ(IUNIT,*) (XA(I), I=1,3), IS(IA)
         ENDDO
      ELSE
         CALL DIE("REDATA: You must specify the atomic coordinates")
      ENDIF


C Read Atomic Species information

      CALL READ_CHEMICAL_TYPES
      NSP = NUMBER_OF_SPECIES()
      IF (NSP.NE.NS) stop 'Error: wrong number of species'

C      write(6,'(/,a)') ' Reading Atomic Species'
C      write(6,'(/,a)') '    Atom    Species'
C      write(6,'(a)')   ' --------------------'
      DO IA = 1, NA
        SYM(IS(IA)) = SPECIES_LABEL (IS(IA))
        Z = ATOMIC_NUMBER(IS(IA))
        AMASS(IS(IA)) = ATMASS(Z)
      ENDDO


C Read Time Step Lenght

      DT = FDF_PHYSICAL('MD.LengthTimeStep',0.0D0,'fs' )
      IF (DT .EQ. 0.0D0) 
     .  stop 'Variable MD.LengthTimeStep not found in input file'
      write(6,'(/,a,f8.5,a)') 
     . ' Length of the time step in the MD run:           ', DT, 'fs'

C Read initial time step for time correlation analisys

      TIMEINI = FDF_INTEGER('AMD.InitialTimeStep',0 )
      write(6,'(/,a,i8)') 
     . ' Initial time step for time correlation analysis:', TIMEINI
      if (TIMEINI .EQ. 0) 
     . write(6,'(a)') ' The first MD time step will be used'

C Read final time step for time correlation analisys

      TIMEFIN = FDF_INTEGER('AMD.FinalTimeStep',0 )
      write(6,'(/,a,i8)') 
     . ' Final time step for time correlation analysis:  ', TIMEFIN
      if (TIMEINI .EQ. 0) write(6,'(a)') 
     .  ' The last MD time step will be used'

      IF (TIMEINI .GT. TIMEFIN) THEN
        WRITE(6,'(/,2a,/)') ' ERROR: ',
     .  'AMD.InitialTimeStep must be smaller than AMD.FinalTimeStep!'
        STOP
      ENDIF


C Read interval for time origins

      IOR = FDF_INTEGER('AMD.TimeSkipInterval',1 )
      write(6,'(/,a,i8)') 
     . ' Interval for time origins in time averages:     ',IOR

C Read length of time for analysis of time correlations

      NT = FDF_INTEGER('AMD.TIMEFUNCS.NTbins',0 )
      write(6,'(/,a,i8)') 
     . ' Number of time steps for time correlations:     ', NT
      if (NT .EQ. 0) write(6,'(a)') 
     . ' The full length of the MD run will be used for time analysis'

C Read number of frequencies for power spectrum

      NW = FDF_INTEGER('AMD.POWER.Nfreq',250 )
      write(6,'(/,a,i8)') 
     . ' Number of frequencies for power spectrum:       ', NW
      if (NW .EQ. 0) write(6,'(a)')
     . ' The default value of 250 is taken'

C Read minimum frequency for power spectrum

      WMIN = FDF_PHYSICAL('AMD.POWER.wmin',0.0D0,'cm-1' )
      write(6,'(/,a,f8.5,a)')
     . ' Minimum frequency for power spectrum:           ', WMIN, 'cm-1'

C Read minimum frequency for power spectrum

      WMAX = FDF_PHYSICAL('AMD.POWER.wmax',1000.0D0,'cm-1' )
      write(6,'(/,a,f8.2,a)')
     . ' Maximum frequency for power spectrum:           ', WMAX, 'cm-1'

C
      RETURN

      END SUBROUTINE REDATACOR


        SUBROUTINE POWER(SPEC, NT, DT, NW, WMIN, WMAX)

C    *******************************************************************
C    ** CALCULATION OF POWER SPECTRUM
C    *******************************************************************


        IMPLICIT NONE

        INTEGER                ::  NT, NW

        REAL(DP)               ::  DT, WMIN, WMAX

        CHARACTER(LEN=10)      :: SPEC

C  Internal variables

        INTEGER                ::  IW, NTIME, IT, IIT, UNIT

        REAL(DP)               ::  W, DW, SUM, PI, FAC, FACTOR, T,
     .                             CONVERT
        REAL(DP)               :: PW
        REAL(DP)               ::  OMEGAWIN, WINDOW

        CHARACTER(LEN=25)      :: FNAME, PASTE

        EXTERNAL PASTE

C
C

        PI = 4.0D0 * ATAN(1.0D0)

C THE FERQUENCY OF THE INTEGRATION WINDOW FUNCTION
C (SEE BELOW)
        OMEGAWIN = 2.0*PI/REAL(2.0*NT)

C FACTOR TO CONVERT FREQUENCIES FROM cm-1 TO fs-1
        CONVERT = 5308.837498D0

C FACTOR FOR INTEGRATION BY SIMPSON'S RULE
        FACTOR = DT / 3.0D0

C OPENS FILE FOR OUTPUT
        FNAME=PASTE(SPEC,'.VDOS')
        CALL IO_ASSIGN(UNIT)
        OPEN(UNIT, FILE=FNAME, STATUS='UNKNOWN')
        REWIND(UNIT)
        WRITE(UNIT,'(a)') '# VDOS: vibrational density of states'
        WRITE(UNIT,'(a)') '# from the FT of the vel-vel autocorr.'
        WRITE(UNIT,'(a)') '#  w(cm-1)   VDOS (states/cm-1)'


        DW = (WMAX - WMIN)/REAL(NW-1)
        W = (WMIN - DW)/CONVERT


        DO IW = 1, NW
          W = W + DW/CONVERT

          SUM = 0.0D0

          DO IT = 1,NT

            T = (IT-1)*DT
            IIT = IT + (NT-2)

C HAMMING WINDOW (TO DUMP OSCILATIONS IN THE FOURIER
C TRANSFORM DUE TO A FINITE TIME WINDOW)

            WINDOW = 0.53836 - 0.46164 * DCOS(OMEGAWIN*IIT)

C FACTORS FOR SIMPSON'S RULE INTEGRATION.
C
C THE FUNCTION IS DEFINED FROM T=0 TO TMAX, BUT THE 
C INTEGRATION IS DONE FROM -TMAX TO TMAX, ASSUMING
C THAT THE FUNCTION IS EVEN (AS IT SHOULD FOR
C THE DEFINITION OF THE V-V AUTOCORRELATION FUNCTION)
C 
C THE FACTORS TAKE INTO ACCOUNT THIS FACT
C
            IF (IT .EQ. NT)  THEN
              FAC = FACTOR
            ELSE IF (MOD(IIT,2) .EQ. 0) THEN
              FAC = 2.0*FACTOR
            ELSE
              FAC = 4.0*FACTOR
            ENDIF
            IF (IT .NE. 1) FAC = 2.0*FAC

            SUM = SUM + FAC*DCOS(W*T)*CF(IT)*WINDOW/CF(1)

          ENDDO

C NORMALIZE POWER SPECTRUM SO THAT THE INTEGRAL OVER
C FREQUENCIES IS ONE

          PW=SUM/PI/CONVERT

          WRITE(UNIT,'(f10.3,f15.10)') W*CONVERT,PW

        ENDDO

        CALL IO_CLOSE(UNIT)

        RETURN
        END SUBROUTINE


      END MODULE DIFFUSION
