#
FC=xlf -q32
FC_ASIS=$(FC)
#
NETCDF_ROOT=/gpfs/projects/ensl61/NETCDF/3.6.2/32
NETCDF_FORTRAN=$(NETCDF_ROOT)
INCFLAGS=-I$(NETCDF_FORTRAN)/include
NETCDF_LIBS= -L$(NETCDF_ROOT)/lib -lnetcdf
#
FFLAGS=-O3 -qstrict -qtune=ppc970 -qarch=ppc970
#
LDFLAGS=
#
.F90.o:
	$(FC) -qsuffix=cpp=F90 -c $(INCFLAGS) $(FFLAGS) $(DEFS) $<
.f90.o:
	$(FC) -qsuffix=f=f90 -c $(INCFLAGS) $(FFLAGS)   $<
.F.o:
	$(FC) -qsuffix=cpp=F -c $(INCFLAGS) -qfixed $(FFLAGS) $(DEFS) $<
.f.o:
	$(FC) -qsuffix=f=f -qfixed -c $(INCFLAGS) $(FFLAGS)   $<
#









