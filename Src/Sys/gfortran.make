#
FC=gfortran
FC_ASIS=$(FC)
#
FFLAGS= -g -O0 -Wall
#
NETCDF_ROOT=$(HOME)/lib/netcdf-3.6.2
NETCDF_FORTRAN=$(NETCDF_ROOT)/gfortran
INCFLAGS=-I$(NETCDF_FORTRAN)/include
#
NETCDF_LIBS= -L$(NETCDF_FORTRAN)/lib -lnetcdff \
             -L$(NETCDF_ROOT)/lib -lnetcdf
#
#FFLAGS= -O2 -mp
#
LDFLAGS=
COMP_LIBS=
#
.F.o:
	$(FC) -c $(FFLAGS)   $<
.f.o:
	$(FC) -c $(FFLAGS)   $<
.F90.o:
	$(FC) -c $(FFLAGS)   $<
.f90.o:
	$(FC) -c $(FFLAGS)   $<
#








